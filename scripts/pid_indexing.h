#include <map>

std::map<int,int> pid_index = { 
   {2212       , 0 } ,
   {1000010020 , 1 } ,
   {1000010030 , 2 } ,
   {1000020030 , 3 } ,
   {1000020040 , 4 }
};

std::map<int,std::string> pid_index_names = { 
   {0, "proton"},    
   {1, "deuteron"},
   {2, "triton"},
   {3, "He3"},
   {4, "alpha"}
};

int GetPIDIndex(int pdg) {
   if( pid_index.count(pdg) == 0) {
      return -1;
   }
   return pid_index[pdg];


}

