
namespace alert {

   bool AcceptThrownParticle(const TParticle& thrown) {
      // returns false if particle does not make it into the acceptance
      //
      using namespace TMath;
      bool accept = true;

      if( thrown.GetPdgCode() == 1000020040 ) {
         if( thrown.P() < 0.250) {
            accept = false;
         }
      } else if( thrown.GetPdgCode() == 1000020030 ) {
         if( thrown.P() < 0.200) {
            accept = false;
         }
      } else if( thrown.GetPdgCode() == 1000010030 ) {
         if( thrown.P() < 0.120) {
            accept = false;
         }
      } else {
         if( thrown.P() < 0.70) {
            accept = false;
         }
      }
      return accept;
   }

   int SmearThrownParticle(const TParticle& thrown, TParticle& smeared) {
      using namespace TMath;
      // assuming out-bend and forward

      double rand = gRandom->Gaus();

      TLorentzVector p_0;
      thrown.Momentum(p_0);
      TLorentzVector p_1;
      thrown.Momentum(p_1);

      //  FoOuMoS1    REA4  .007       Forward outbending mometum sigma_1
      //  FoOuMoS2    REA4  .0         Forward outbending mometum sigma_2
      //  FoOuThS1    REA4  .0286      Forward outbending Theta sigma_1
      //  FoOuThS2    REA4  .00        Forward outbending Theta sigma_2
      //  FoA0FiS1    REA4  .3125E-2   Forward in(out)bending  A0 phi sigma_1
      //  FoA1FiS1    REA4  .262E-4    Forward in(out)bending  A1 phi sigma_1
      //  FoA2FiS1    REA4  -.350E-6   Forward in(out)bending  A2 phi sigma_1
      //
      std::vector<double> forward_pars = {
         0.007     ,  //Forward outbending mometum sigma_1
         0.0       ,  //Forward outbending mometum sigma_2
         0.0286    ,  //Forward outbending Theta sigma_1
         0.00      ,  //Forward outbending Theta sigma_2
         0.3125e-2 ,  //Forward in(out)bending  A0 phi sigma_1
         0.262e-4  ,  //Forward in(out)bending  A1 phi sigma_1
         -0.350e-6     //Forward in(out)bending  A2 phi sigma_1
      };

      //           sig1p=FoOuMoS1*Tmax/torcur
      //           sig2p=FoOuMoS2*Tmax/torcur
      //           sig1th=FoOuThS1
      //           sig2th=FoOuThS2
      //           sig1fi=2.*sig1th
      //           sig2fi=2.*sig2th
      //           pout=p+ran(1)*p*sqrt((sig1p*p)**2+(sig2p/beta)**2)
      //           thout=thr+ran(2)*sqrt(sig1th**2+(sig2th/p/beta)**2)
      //           phiout=phir+ran(3)*sqrt(sig1fi**2+(sig2fi/p/beta)**2)

      double sig1p  = forward_pars[0];//*Tmax/torcur
      double sig2p  = forward_pars[1];//*Tmax/torcur
      double sig1th = forward_pars[2];
      double sig2th = forward_pars[3];
      double sig1fi = 2.0*sig1th;
      double sig2fi = 2.0*sig2th;

      double p      = p_0.P();
      double thr    = p_0.Theta();
      double phir   = p_0.Phi();
      double beta   = p_0.Beta();

      double pout   = p    + gRandom->Gaus()*p*Sqrt((sig1p*p)*(sig1p*p)+(sig2p/beta)*(sig2p/beta));
      double thout  = thr  + gRandom->Gaus()*Sqrt(Power(sig1th*sig1th+(sig2th/p/beta),2));
      double phiout = phir + gRandom->Gaus()*Sqrt(Power(sig1fi*sig1fi+(sig2fi/p/beta),2));

      p_0.SetRho(   pout );
      p_0.SetTheta( thout );
      p_0.SetPhi(   phiout );

      //std::cout << p <<std::endl;
      //std::cout << pout <<std::endl;

      smeared.SetMomentum(p_0);
      //p_1.Print();
      //p_0.Print();

      return 0;
   }
}
