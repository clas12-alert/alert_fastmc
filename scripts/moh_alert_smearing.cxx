#include <iostream>
#include <fstream>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <math.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TF1.h>
#include <TFile.h>
#include <time.h>
#include <TMultiGraph.h>
#include <TVector3.h>
#include <TMath.h>
#include <TProfile.h>
#include "TCutG.h"
#include <TGraphErrors.h>
#include <TLatex.h>
#include "TGraph.h"
#include<TLine.h>
#include <TLorentzVector.h>
#include "TSystem.h"
#include "TLegend.h"
#include<algorithm>
#include<vector>

#include "ThrownEvent.h"
#include "CLAS12HitsEvent.h"
#include "ForwardHitMask.h"
#include "ElectronKinematics.h"

#include "pid_indexing.h"
#include "CLAS12Resolution.h"
#include "ALERTResolution.h"

// ----------------------------------------------------------------------
//
// alpha     = 1000020040
// He3       = 1000020030
// triton    = 1000010030
// deuteron  = 1000010020
// proton    = 2212
//

double Find_PHI_1(TLorentzVector, TLorentzVector, TLorentzVector, TLorentzVector);

//  electron distributions
TH2D* h_Ps_Theta_e = new TH2D("h_Ps_Theta_e","Electrons: E vs. #theta", 250, 0, 45, 250,-2,13);
TH2D* h_Phis_Thetas_e = new TH2D("","DVCS electrons; ; #phi [Deg.]; ; #theta [Deg.]", 250,-175,185, 250,0, 45);
   // resolution distributions 
   TH2D* h_DeltaP_p_e = new TH2D("","e': #Deltap/p vs. p; ;[GeV/c]",250,0,12, 250, -0.2, 0.2);
   TH2D* h_DeltaTheta_p_e = new TH2D("","e': #Delta#theta vs. p; [Deg.];[GeV/c]",250,0,12, 250, -5, 5);
   TH2D* h_DeltaPhi_p_e = new TH2D("","e': #Delta#phi vs. p; [Deg.];[GeV/c]",250,0,12, 250, -5, 5 );


// photon distributions
TH2D* h_Ps_Theta_phot = new TH2D("","Photons: E vs. #theta;[Deg.]; [GeV]",250,0,40, 250,0,13);
TH2D* h_Phis_Thetas_phot = new TH2D("","DVCS photons; #phi [Deg.]; #theta [Deg.]", 250,-175,185, 250, 0, 40);
   // resolution distributions 
   TH2D* h_DeltaP_p_phot = new TH2D("","#gamma: #Delta E vs. E; ;[GeV]",250,0,12, 250, -0.5, 0.5);
   TH2D* h_DeltaTheta_p_phot = new TH2D("","#gamma: #Delta#theta vs. E;[GeV]; [Deg.]",250,0,12, 250,-5,5);
   TH2D* h_DeltaPhi_p_phot = new TH2D("","#gamma: #Delta#phi vs. E; [GeV/c]; [Deg.]",250,0,12,250,-5,5);


// Helium distributions
TH2D* h_Ps_Theta_he = new TH2D("","^{4}He: p vs. #theta;[Deg.]; [MeV/c.]",250, 2, 100, 250, 150, 450);
TH2D* h_Phis_Thetas_he = new TH2D("","^{4}He ; #phi [Deg.]; #theta [Deg.]", 250,-175,185, 250, 2, 100);

   // resolution distributions 
   TH1D* h_DeltaP_he = new TH1D("","^{4}He: (p_{Rec.} - p_{Gen.})/p_{Gen.} ; ;",250,-0.3,0.3);
   TH1D* h_DeltaPhi_he = new TH1D("","^{4}He: #Delta#phi ;[Deg.]; ", 250,-10,10);
   TH1D* h_DeltaTheta_he = new TH1D("","^{4}He: #Delta#theta; [deg.];", 250,-10,10);

TH2D *h_Q2_xB_Coh = new TH2D("","Q^{2} vs. x_{B}; ; [GeV^{2}/c^{2}]",250, 0, 0.5, 250, 0, 8);
TH2D *h_t_Q2_Coh  = new TH2D("","-t vs. Q^{2}; [GeV^{2}/c^{2}]; [GeV^{2}/c^{2}]",250, 0.75, 13.0, 250, 0, 1.0);
TH2D *h_phi_t_Coh = new TH2D("","-t vs. #phi; [deg.]; [GeV^{2}/c^{2}]",250, 0, 360, 250, 0, 0.8);





void moh_alert_smearing(int runnumber = 50)
{
   using namespace clas12::hits;

   gROOT->Reset();
   gStyle->SetOptStat(kFALSE);
   //  gStyle->SetPalette(55);   
/*
   gStyle->SetLabelSize(0.03,"xyz"); // size of axis value font 
   gStyle->SetTitleSize(0.035,"xyz"); // size of axis title font 
   gStyle->SetTitleFont(22,"xyz"); // font option 
   gStyle->SetLabelFont(22,"xyz");
   gStyle->SetTitleOffset(1.2,"y");
   gStyle->SetCanvasBorderMode(0);
   gStyle->SetCanvasBorderSize(0);
   gStyle->SetPadBottomMargin(0.16); //margins... 
   gStyle->SetPadTopMargin(0.16);
   gStyle->SetPadLeftMargin(0.08);
   gStyle->SetPadRightMargin(0.1);
   gStyle->SetFrameBorderMode(0);
   gStyle->SetPaperSize(20,24);
   gStyle->SetLabelSize(0.05,"xy");
   gStyle->SetTitleSize(0.06,"xy");
*/
   std::string filename     = Form("../data/rootfiles/clas12sim%d.root",runnumber);
   std::string filename_out = Form("moh_out_c12sim_smeared_%d.root",runnumber);


   TCanvas *c1 = new TCanvas("c1");
   TLine *l = new TLine();
   l->SetLineWidth(4);
   l->SetLineColor(2);
   l->SetLineStyle(7);

   // ----------------------------------------------------------------------
   //
   TFile * f = new TFile(filename.c_str(),"READ");
   f->cd();
   f->ls();
   TTree * t = (TTree*)gROOT->FindObject("clasdigi_hits"); 
   if(!t) {
      std::cout << "error tree not found\n";
      return -1;
   }
   clas12::hits::CLAS12HitsEvent  * event   = 0;
   clas12::sim::ThrownEvent       * thrown  = 0;//new clas12::sim::ThrownEvent();
   kinematics::ElectronKinematics * eKine0  = new kinematics::ElectronKinematics();
   clas12::sim::ThrownEvent       * smeared = new clas12::sim::ThrownEvent();
   kinematics::ElectronKinematics * eKine  = new kinematics::ElectronKinematics();

   t->SetBranchAddress("HitsEvent",   &event);
   t->SetBranchAddress("PrimaryEvent",&thrown);

   // ----------------------------------------------------------------------
   //
   TFile * fout = new TFile(filename_out.c_str(),"RECREATE");
   fout->cd();

   TTree * tout  = new TTree("FastMC","fast mc smeared");
   Int_t alert_accepted = 0;
   tout->Branch("HitsEvent", "clas12::hits::CLAS12HitsEvent", &event);
   tout->Branch("Thrown",    "clas12::sim::ThrownEvent",      &thrown);
   tout->Branch("Smeared",   "clas12::sim::ThrownEvent",      &smeared);
   tout->Branch("eKine0",   "kinematics::ElectronKinematics", &eKine0);
   tout->Branch("eKine",   "kinematics::ElectronKinematics", &eKine);
   tout->Branch("alert_accepted", &alert_accepted, "alert_accepted/I");

   TRandom3 rand;
   rand.SetSeed();

   eKine->fM1  = 0.938;
   eKine0->fM1 = 0.938;
   eKine->Get_k1()  = { 0.0, 0.0, 11.0, 11.0 };
   eKine0->Get_k1() = { 0.0, 0.0, 11.0, 11.0 };
   eKine->Get_p1()  = { 0.0, 0.0, 0.0, 0.938 };
   eKine0->Get_p1() = { 0.0, 0.0, 0.0, 0.938 };

   // ----------------------------------------------------------------------
   //
   int nevents = t->GetEntries();
   for(int ievent = 0; ievent < nevents; ievent++){

      smeared->Clear();
      alert_accepted = 0;
      //mask->Clear();

      t->GetEntry(ievent);
   
      //---------------------------------------------------------------------
      double R2D = 180./3.14;
      double D2R = 3.14/180.;
      double Eb= 11.0;       //in Gev
      double M_4He= 3.727;    //in GeV   
      double M_p= 0.93827;      //in GeV
      double M_pi= 0.13957;    // mass of pi(+/-) in GeV
      double M_e= 0.000511;
      double LightSpeed = 29.9792458;        // cm per ns 
   
      TLorentzVector  InEl4Vector;       InEl4Vector.SetPxPyPzE(0, 0, Eb, Eb);
      TLorentzVector  TargetPVector;     TargetPVector.SetPxPyPzE(0, 0, 0, M_p);
      TLorentzVector  Target4HeVector;   Target4HeVector.SetPxPyPzE(0., 0., 0., M_4He);
      TLorentzVector  GammaStar4Vector;
      TLorentzVector  Elec4Vector;
      TLorentzVector  He4Vector;
      TLorentzVector  Photon4Vector;
   
      // smeared electron, He4 and photon
      TLorentzVector  S_Elec4Vector;
      TLorentzVector  S_He4Vector;
      TLorentzVector  S_Photon4Vector;   

      // ---------------------------------------------------------
      // Event info
      thrown->fRunNumber           = event->fRunNumber;
      event->fHitMask.fRunNumber   = event->fRunNumber;
      event->fHitMask.fEventNumber = event->fEventNumber;

      // ---------------------------------------------------------
      // Count the hits
      int n_dc_hits = event->fDCEvent.fNParticleHits;
      int n_ec_hits = event->fECEvent.fNRegionHits;
      int n_rc_hits = event->fRCEvent.fNParticleHits;
      int n_rh_hits = event->fRecoilScintEvent.fNParticleHits;
      int n_thrown  = thrown->fNParticles;

      //std::cout << " DC hits \n";
      // ---------------------------------------------------------
      // DC
      for(int ihit = 0; ihit < n_dc_hits; ihit++){
         auto phit     = event->fDCEvent.GetParticleHit(ihit);
         int  track_id = phit->fTrackID - 1;

         int sector = phit->fDCWire.fSector;
         event->fHitMask.fDCSectors[sector]++;
         event->fHitMask.fDC++;
      }

      // std::cout << "event->fHitMask.fDC = "<< event->fHitMask.fDC<<endl;

      //std::cout << " EC hits \n";
      // ---------------------------------------------------------
      // EC  
      for(int ihit = 0; ihit < n_ec_hits; ihit++){
         auto phit = event->fECEvent.GetRegionHit(ihit);
         if(phit->fPDGCode == 11) {
            event->fHitMask.fElectron++;
            //alert_accepted++;
            event->fHitMask.fElectron++;
         }
         event->fHitMask.fEC++;
      }

      if( event->fHitMask.fEC ) {
            event->fHitMask.fForwardStatus = 1;
      }

      // ---------------------------------------------------------
      // RC  
      //std::cout << " RC hits \n";
      for(int ihit = 0; ihit < n_rc_hits; ihit++)
      {
         auto * phit     = event->fRCEvent.GetParticleHit(ihit);
         int    track_id = phit->fTrackID - 1;

         if( track_id < n_thrown ) {
            auto part = thrown->GetParticle( track_id );
            if( (part->GetPdgCode() == phit->fPDGCode) && (phit->fPDGCode != 11) ) {
               event->fHitMask.fRC++;
            }
         }
      }

      // ---------------------------------------------------------
      // RH  
      //std::cout << " RH hits \n";
      for(int ihit = 0; ihit < n_rh_hits; ihit++){
         //ParticleHit * phit = event->fRHEvent.GetParticleHit(ihit);
         //if(phit->fPDGCode == pdg_select) {
         //   alert_accepted++;
         //}
         event->fHitMask.fRH++;
      }

      if( event->fHitMask.fRC ) {
            event->fHitMask.fCentralStatus = 1;
      }

      event->fHitMask.fStatus  = (event->fHitMask.fForwardStatus && event->fHitMask.fCentralStatus);


   if(event->fHitMask.fStatus ) //> 0 && event->fHitMask.fDC > 30 && event->fHitMask.fEC> 1 ) 
    { 
      // ---------------------------------------------------------
      // Smeared event

       int acc_elec= 0;
       int acc_phot= 0;
       int acc_He= 0;

       for(int ithrown = 0; ithrown<n_thrown; ithrown++)
       {

         auto p0 = thrown->GetParticle(ithrown);
         smeared->AddParticle(p0);


         auto p1 = smeared->GetParticle(ithrown);

         // electron part 
         if( p0->GetPdgCode() ==   11 ) {
            acc_elec= 1;
            p0->Momentum( Elec4Vector   ) ;
            p1->Momentum( S_Elec4Vector );

            h_Ps_Theta_e         ->Fill(Elec4Vector.Theta()*R2D, Elec4Vector.P());
            h_Phis_Thetas_e->Fill(Elec4Vector.Phi()*R2D, Elec4Vector.Theta()*R2D);
            h_DeltaP_p_e      ->Fill(Elec4Vector.P(), (S_Elec4Vector.P() - Elec4Vector.P())/Elec4Vector.P());
            h_DeltaTheta_p_e  ->Fill(Elec4Vector.P(), (S_Elec4Vector.Theta() - Elec4Vector.Theta())*R2D);
            h_DeltaPhi_p_e    ->Fill(Elec4Vector.P(), (S_Elec4Vector.Phi() - Elec4Vector.Phi())*R2D);
         }
   
          // photon part
          if( p0->GetPdgCode() == 22   ) {
             acc_phot= 1;
             p0->Momentum( Photon4Vector   ) ;
             p1->Momentum( S_Photon4Vector ) ; 

             h_Ps_Theta_phot         ->Fill(S_Photon4Vector.Theta()*R2D, S_Photon4Vector.E()); 
             h_Phis_Thetas_phot->Fill(S_Photon4Vector.Phi()*R2D, S_Photon4Vector.Theta()*R2D);
 	     h_DeltaP_p_phot   ->Fill(Photon4Vector.E(),(S_Photon4Vector.E()-Photon4Vector.E()));

             h_DeltaTheta_p_phot->Fill(Photon4Vector.E(), (S_Photon4Vector.Theta() - Photon4Vector.Theta())*R2D);
             h_DeltaPhi_p_phot  ->Fill(Photon4Vector.E(), (S_Photon4Vector.Phi() - Photon4Vector.Phi())*R2D);
          }
 
          // helium part 
          if( p0->GetPdgCode() == 1000020040 ) {
             acc_He= 1 ;
             p0->Momentum( He4Vector );
             p1->Momentum( S_He4Vector );

             h_Ps_Theta_he         ->Fill(S_He4Vector.Theta()*R2D, S_He4Vector.P()*1000); 
             h_Phis_Thetas_he->Fill(S_He4Vector.Phi()*R2D, S_He4Vector.Theta()*R2D);
           
             h_DeltaP_he      ->Fill((S_He4Vector.P() - He4Vector.P())/He4Vector.P());
             h_DeltaPhi_he    ->Fill(S_He4Vector.Phi()*R2D - He4Vector.Phi()*R2D );
             h_DeltaTheta_he  ->Fill( S_He4Vector.Theta()*R2D - He4Vector.Theta()*R2D);
         }
       }


    if (acc_elec==1 && acc_phot==1 && acc_He==1)
        {
           double Q2 = 4*S_Elec4Vector.P()*Eb*pow(sin(S_Elec4Vector.Theta()/2), 2);
           double nu = (Eb - S_Elec4Vector.P());
          // double yy = nu/Eb;
          // double W_4He = sqrt(M_4He*M_4He + 2*M_4He*nu - Q2);
          // double W_p = sqrt(M_p*M_p + 2*M_p*nu - Q2);
           double phi_coh = Find_PHI_1(InEl4Vector, S_Elec4Vector, S_He4Vector, S_Photon4Vector);
           double xB  = Q2/(2*M_p*nu);
           double t_coh = -1*(Target4HeVector - He4Vector).M2();
           h_Q2_xB_Coh ->Fill(xB, Q2);
           h_t_Q2_Coh  ->Fill(Q2, t_coh); 
           h_phi_t_Coh ->Fill(phi_coh, t_coh); 
        }




     }



      for(int ithrown = 0; ithrown<n_thrown; ithrown++)
      {

         auto p0 = thrown->GetParticle(ithrown);
         smeared->AddParticle(p0);

         auto p1 = smeared->GetParticle(ithrown);
         TLorentzVector  P_smeared;
         p1->Momentum( P_smeared );

         if( p0->GetPdgCode() ==   11 ) {
            eKine0->Get_k2() = P_smeared;
         }

         clas12::SmearThrownParticle(*p0, *p1);

         if( p1->GetPdgCode() ==   11 )
         {
            eKine->Get_k2() = P_smeared;
         }
         else if( (p1->GetPdgCode() == 2212) && (p1->GetPdgCode() == 22) )
         {
            //p1->Print();
            eKine->Get_p2() = P_smeared;
         }
         else if( (p1->GetPdgCode() == 1000020040) )
         {
            //p1->Print();
            eKine->Get_p2() = P_smeared;
         }
      }

      //event->Print();
      //thrown->Print();
      tout->Fill();
   }

   fout->Write();

c1->cd();

h_Ps_Theta_e       ->Draw("colz");    c1->Print("fig/el_Ps_Theta.png");
h_Phis_Thetas_e    ->Draw("colz");    c1->Print("fig/el_Phis_Thetas.png");
h_DeltaP_p_e       ->Draw("colz");    c1->Print("fig/el_DeltaP_p.png");
h_DeltaTheta_p_e   ->Draw("colz");    c1->Print("fig/el_DeltaTheta_p.png");
h_DeltaPhi_p_e     ->Draw("colz");    c1->Print("fig/el_DeltaPhi_p.png");

h_Ps_Theta_phot    ->Draw("colz");    c1->Print("fig/phot_Ps_Theta.png");
h_Phis_Thetas_phot ->Draw("colz");    c1->Print("fig/phot_Phis_Thetas.png");
h_DeltaP_p_phot    ->Draw("colz");    c1->Print("fig/phot_DeltaP_p.png");
h_DeltaTheta_p_phot->Draw("colz");    c1->Print("fig/phot_DeltaTheta_p.png");
h_DeltaPhi_p_phot  ->Draw("colz");    c1->Print("fig/phot_DeltaPhi_p.png");

h_Ps_Theta_he      ->Draw("colz");    c1->Print("fig/he_Gen_Ps_Theta.png");
h_Phis_Thetas_he   ->Draw("colz");    c1->Print("fig/he_Phis_Thetas.png");
h_DeltaP_he        ->Draw(); c1->Print("fig/he_DeltaP.png"); c1->Print("fig/he_DeltaP.C");
h_DeltaPhi_he      ->Draw(); c1->Print("fig/he_DeltaPhi.png");  c1->Print("fig/he_DeltaPhi.C");
h_DeltaTheta_he    ->Draw(); c1->Print("fig/he_DeltaTheta.png");  c1->Print("fig/he_DeltaTheta.C");

h_Q2_xB_Coh        ->Draw("colz");          c1->Print("fig/coh_Q2_xB.png");          
h_t_Q2_Coh         ->Draw("colz");          c1->Print("fig/coh_t_Q2.png");
h_phi_t_Coh        ->Draw("colz");          c1->Print("fig/coh_phi_t.png");

}


//////////////////////////////////////////////////////////////////////////////////////////////////////
/////// function to find the phi between the leptonic and Hadronic planes //////////
//using proton and gamma_star

double Find_PHI_1(TLorentzVector InEl4Vector, TLorentzVector Elec4Vector, TLorentzVector Proton4Vector, TLorentzVector Photon4Vector)
{
  double PHI_h= .0;
  //double TODEG = 180.0/3.14;
  TLorentzVector GammaStar4Vector = InEl4Vector - Elec4Vector;
  TVector3 LeptonicPlane = (InEl4Vector.Vect()).Cross(Elec4Vector.Vect());
  TVector3 HadronicPlane = (Proton4Vector.Vect()).Cross(GammaStar4Vector.Vect());

  PHI_h = LeptonicPlane.Angle(HadronicPlane)*TODEG;
  if(LeptonicPlane.Dot(Proton4Vector.Vect())<0.)  PHI_h = 360.0 - PHI_h;

 return PHI_h;
}



