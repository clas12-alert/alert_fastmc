#include "ThrownEvent.h"
#include "CLAS12HitsEvent.h"
#include "ForwardHitMask.h"
#include "ElectronKinematics.h"
#include "CLHEP/Units/SystemOfUnits.h"

#include "../pid_indexing.h"
#include "../ALERTResolution.h"
#include "../CLAS12Resolution.h"

// ----------------------------------------------------------------------
//
// alpha     = 1000020040
// He3       = 1000020030
// triton    = 1000010030
// deuteron  = 1000010020
// proton    = 2212
//
void tagged_dvcs_He4_proton(int runnumber = 2)
{
   using namespace clas12::hits;
   using namespace CLHEP;

   TF1 * fA_LU2 = new TF1("ALU","0.5+(0.1-0.01*[0])*sin(x*3.1451/180.0)",-180,180);
   TF1 * fA_LU = new TF1("ALU2","0.5+0.1+[0]-[0]",-180,180);

   TF1 * myfunc = new TF1("ALUfit","[0]*sin(x*3.1451/180.0)/(1.0-[1]*cos(x*3.1451/180.0))",-180,180);
   myfunc->SetParLimits(0,-2,2);
   myfunc->SetParLimits(1,-0.1,0.1);

   std::vector<int> runs = {21021,20500,20501,20502,20600,20700,20800,20900,21000};
   //std::vector<int> runs = {1,50000};

   std::vector<double> Q2bins = {1    , 1.5 , 2.0 , 3.0    , 6};
   std::vector<double> tbins  = {0    , 1.0  , 2.0 , 3.0 , 6.0};
   std::vector<double> xbins  = {0.05 , 0.25 , 0.35, 0.5 , 0.8};
   std::vector<double> P1bins = {0.0 , 0.2 , 0.35, 0.5, 1.0};
   std::vector<double> thetaP1bins = {0.0 , 50 , 100, 180, 180};

   std::string dirname      = Form("data/results/fastmc/tagged_dvcs_He4_proton_%d",runnumber);
   std::string filename     = Form("data/rootfiles/c12sim_smeared_%d.root",runnumber);
   std::string filename_out = Form("data/rootfiles/c12_smeared_%d.root",runnumber);

   gSystem->mkdir(dirname.c_str(),true);

   // ----------------------------------------------------------------------
   //
   clas12::hits::CLAS12HitsEvent * event   = 0;
   clas12::sim::ThrownEvent      * thrown  = 0;//new clas12::sim::ThrownEvent();
   clas12::sim::ThrownEvent      * smeared = new clas12::sim::ThrownEvent();
   kinematics::ElectronKinematics * eKine0 = new kinematics::ElectronKinematics();
   kinematics::ElectronKinematics * eKine  = new kinematics::ElectronKinematics();

   TChain * t = new TChain("FastMC");
   for(auto arun : runs ){
      filename     = Form("data/rootfiles/c12sim_smeared_%d.root",arun);
      t->Add(filename.c_str());
   }

   //t->SetBranchAddress("HitsEvent",   &event);
   //t->SetBranchAddress("PrimaryEvent",&thrown);

   // ----------------------------------------------------------------------
   //
   //TFile * f = new TFile(filename.c_str(),"READ");
   //f->cd();

   //TTree * t = (TTree*)gROOT->FindObject("FastMC"); 
   Int_t alert_accepted = 0;
   t->SetBranchAddress("HitsEvent", &event);
   t->SetBranchAddress("Thrown",    &thrown);
   t->SetBranchAddress("Smeared",   &smeared);
   t->SetBranchAddress("eKine0",     &eKine0);
   t->SetBranchAddress("eKine",     &eKine);

   TRandom3 rand;
   rand.SetSeed();

   // ----------------------------------------------------------------------
   //
   TFile * fout = new TFile(filename_out.c_str(),"RECREATE");
   

   std::vector<TH1F*> hDeltaE = {
      new TH1F("hDeltaE0", "E", 100, -1, 1),
      new TH1F("hDeltaE1", "E", 100, -1, 1),
      new TH1F("hDeltaE2", "E", 100, -1, 1),
      new TH1F("hDeltaE3", "E", 100, -1, 1),
      new TH1F("hDeltaE4", "E", 100, -1, 1)
   };
   std::vector<TH1F*> hDeltaTheta = {
      new TH1F("hDeltaTheta0", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta1", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta2", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta3", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta4", "#Delta#theta", 100, -10, 10)
   };

   std::vector<TH1F*> hPhi = {
      new TH1F("hPhi0", "#phi", 100, 0, 360),
      new TH1F("hPhi1", "#phi", 100, 0, 360),
      new TH1F("hPhi2", "#phi", 100, 0, 360),
      new TH1F("hPhi3", "#phi", 100, 0, 360),
      new TH1F("hPhi4", "#phi", 100, 0, 360)
   };


   // integrated over all p1
   std::vector<std::vector<std::vector<std::vector<TH1F*> >>> hPhiAsymBinned;
   for(int i = 0;i<5;i++) {
      std::vector<std::vector<std::vector<TH1F*> > > vecx;
      hPhiAsymBinned.push_back( vecx ) ;
      for(int i2 = 0;i2<5;i2++) {
         std::vector<std::vector<TH1F*>> vecq2;
         hPhiAsymBinned[i].push_back(vecq2 ) ;
         for(int i3 = 0;i3<5;i3++) {
            std::vector<TH1F*> vect; 
            hPhiAsymBinned[i][i2].push_back(vect);
            for(int i4 = 0;i4<5;i4++) {
               hPhiAsymBinned[i][i2][i3].push_back(
                     new TH1F(Form("hPhiAsym%d%d%d%d",i4,i,i2,i3), "#phi", 12, -180, 180)
                     );
            }
         }
      }
   }
   // p1 binned (4 dimensional)
   std::vector<std::vector<std::vector<std::vector<std::vector<TH1F*> >>>> hPhiAsymBinnedP1;
   std::vector<std::vector<std::vector<std::vector<std::vector<double> >>>> fitAlpha;
   std::vector<std::vector<std::vector<std::vector<std::vector<double> >>>> fitAlphaErr;
   for(int i = 0;i<5; i++) {
      std::vector<std::vector<std::vector<std::vector<TH1F*> >>> vecx;
      std::vector<std::vector<std::vector<std::vector<double> >>> fitx;
      hPhiAsymBinnedP1.push_back( vecx ) ;
      fitAlpha.push_back( fitx ) ;
      fitAlphaErr.push_back( fitx ) ;
      for(int i2 = 0;i2<5; i2++) {
         std::vector<std::vector<std::vector<TH1F*>>> vecq2;
         std::vector<std::vector<std::vector<double>>> fitq2;
         hPhiAsymBinnedP1[i].push_back(vecq2 ) ;
         fitAlpha[i].push_back( fitq2 ) ;
         fitAlphaErr[i].push_back( fitq2 ) ;
         for(int i3 = 0;i3<5; i3++) {
            std::vector<std::vector<TH1F*>> vect; 
            std::vector<std::vector<double>> fitt; 
            hPhiAsymBinnedP1[i][i2].push_back(vect);
            fitAlpha[i][i2].push_back(fitt);
            fitAlphaErr[i][i2].push_back(fitt);
            for(int i4 = 0;i4<5; i4++) {
               std::vector<TH1F*> vecP1; 
               std::vector<double> fitP1; 
               hPhiAsymBinnedP1[i][i2][i3].push_back(vecP1);
               fitAlpha[i][i2][i3].push_back(fitP1);
               fitAlphaErr[i][i2][i3].push_back(fitP1);
               for(int i5 = 0;i5<5; i5++) {
                  hPhiAsymBinnedP1[i][i2][i3][i4].push_back(
                        new TH1F(Form("hPhiP1Asym%d%d%d%d%d",i4,i,i2,i3,i5), "#phi", 12, -180, 180)
                        );
                  fitAlpha[i][i2][i3][i4].push_back(0.0);
                  fitAlphaErr[i][i2][i3][i4].push_back(0.0);
               }
            }
         }
      }
   }

   // p1 binned (5 dimensional)
   std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<TH1F*> >>>>> hPhiAsymBinnedP1ThetaP1;
   std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<double> >>>>> fitAlpha2;
   std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<double> >>>>> fitAlphaErr2;
   std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<double> >>>>> fitAlphaRatio2;
   std::vector<std::vector<std::vector<std::vector<std::vector<std::vector<double> >>>>> fitAlphaRatioErr2;
   for(int i = 0;i<5; i++) {
      std::vector<std::vector<std::vector<std::vector<std::vector<TH1F*> >>>> vecx;
      std::vector<std::vector<std::vector<std::vector<std::vector<double> >>>> fitx;
      hPhiAsymBinnedP1ThetaP1.push_back( vecx ) ;
      fitAlpha2.push_back( fitx ) ;
      fitAlphaErr2.push_back( fitx ) ;
      fitAlphaRatio2.push_back( fitx ) ;
      fitAlphaRatioErr2.push_back( fitx ) ;
      for(int i2 = 0;i2<5; i2++) {
         std::vector<std::vector<std::vector<std::vector<TH1F*>>>> vecq2;
         std::vector<std::vector<std::vector<std::vector<double>>>> fitq2;
         hPhiAsymBinnedP1ThetaP1[i].push_back(vecq2 ) ;
         fitAlpha2[i].push_back( fitq2 ) ;
         fitAlphaErr2[i].push_back( fitq2 ) ;
         fitAlphaRatio2[i].push_back( fitq2 ) ;
         fitAlphaRatioErr2[i].push_back( fitq2 ) ;
         for(int i3 = 0;i3<5; i3++) {
            std::vector<std::vector<std::vector<TH1F*>>> vect; 
            std::vector<std::vector<std::vector<double>>> fitt; 
            hPhiAsymBinnedP1ThetaP1[i][i2].push_back(vect);
            fitAlpha2[i][i2].push_back(fitt);
            fitAlphaErr2[i][i2].push_back(fitt);
            fitAlphaRatio2[i][i2].push_back(fitt);
            fitAlphaRatioErr2[i][i2].push_back(fitt);
            for(int i4 = 0;i4<5; i4++) {

               std::vector<std::vector<TH1F*>> vecP1; 
               std::vector<std::vector<double>> fitP1; 
               hPhiAsymBinnedP1ThetaP1[i][i2][i3].push_back(vecP1);
               fitAlpha2[i][i2][i3].push_back(fitP1);
               fitAlphaErr2[i][i2][i3].push_back(fitP1);
               fitAlphaRatio2[i][i2][i3].push_back(fitP1);
               fitAlphaRatioErr2[i][i2][i3].push_back(fitP1);

               for(int i5 = 0;i5<5; i5++) {
                  std::vector<TH1F*> vecthetaP1; 
                  std::vector<double> fitthetaP1; 
                  hPhiAsymBinnedP1ThetaP1[i][i2][i3][i4].push_back(vecthetaP1);
                  fitAlpha2[i][i2][i3][i4].push_back(fitthetaP1);
                  fitAlphaErr2[i][i2][i3][i4].push_back(fitthetaP1);
                  fitAlphaRatio2[i][i2][i3][i4].push_back(fitthetaP1);
                  fitAlphaRatioErr2[i][i2][i3][i4].push_back(fitthetaP1);

                  for(int i6 = 0;i6<4; i6++) {
                     hPhiAsymBinnedP1ThetaP1[i][i2][i3][i4][i5].push_back(
                           new TH1F(Form("hPhithetaP1Asym%d%d%d%d%d%d",i4,i,i2,i3,i5,i6), "#phi", 12, -180, 180)
                           );
                     fitAlpha2[i][i2][i3][i4][i5].push_back(0.0);
                     fitAlphaErr2[i][i2][i3][i4][i5].push_back(0.0);
                     fitAlphaRatio2[i][i2][i3][i4][i5].push_back(0.0);
                     fitAlphaRatioErr2[i][i2][i3][i4][i5].push_back(0.0);
                  }
               }
            }
         }
      }
   }
   std::vector<TH1F*> hPhiAsym = {
      new TH1F("hPhiAsym0", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym1", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym2", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym3", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym4", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym5", "#phi", 20, -180, 180)
   };

   std::vector<TH2F*> hThetaPhi = {
      new TH2F("hThetaPhi0", "#theta vs #phi", 180, 0, 360, 50, 0, 50),
      new TH2F("hThetaPhi1", "#theta vs #phi", 180, 0, 360, 50, 0, 50),
      new TH2F("hThetaPhi2", "#theta vs #phi", 180, 0, 360, 50, 0, 50),
      new TH2F("hThetaPhi3", "#theta vs #phi", 180, 0, 360, 90, 0, 180),
      new TH2F("hThetaPhi4", "#theta vs #phi", 180, -180, 180, 50, 0, 50),
      new TH2F("hThetaPhi5", "#theta vs #phi", 180, -180, 180, 50, 0, 50),
      new TH2F("hThetaPhi6", "#theta vs #phi", 180, -180, 180, 50, 0, 50),
      new TH2F("hThetaPhi7", "#theta vs #phi", 180, -180, 180, 90, 0, 180),
      new TH2F("hThetaPhi8", "#theta vs #phi", 180, 0, 360, 90, 0, 180)
   };
   std::vector<TH2F*> hThetaP = {
      new TH2F("hThetaP0", "#theta vs P", 100, 0, 11, 40, 0, 50),
      new TH2F("hThetaP1", "#theta vs P", 100, 0, 11, 70, 0, 50),
      new TH2F("hThetaP2", "#theta vs P", 100, 0, 11, 70, 0, 50),
      new TH2F("hThetaP3", "#theta vs P", 100, 0, 1,  90, 0, 180),
      new TH2F("hThetaP4", "#theta vs P", 100, 0, 11, 70, 0, 50),
      new TH2F("hThetaP5", "#theta vs P", 100, 0, 11, 70, 0, 50),
      new TH2F("hThetaP6", "#theta vs P", 100, 0, 11, 70, 0, 50),
      new TH2F("hThetaP7", "#theta vs P", 100, 0, 1,  90, 0, 180),
      new TH2F("hThetaP8", "#theta vs P", 100, 0, 11, 90, 0, 180)
   };

   std::vector<TH2F*> hP1Vsx = {
      new TH2F("hP1Vsx0", "P_{1} Vs x", 100, 0,1, 100, 0, 1),
      new TH2F("hP1Vsx1", "P_{1} Vs x", 100, 0,1, 100, 0, 1),
      new TH2F("hP1Vsx2", "P_{1} Vs x", 100,-1,1, 100, -1, 1),
      new TH2F("hP1Vsx3", "P_{1} Vs x", 100, 0,1, 100, 0, 1),
      new TH2F("hP1Vsx4", "P_{1} Vs x", 100, 0,1, 100, 0, 1)
   };
   std::vector<TH2F*> hP1Vst = {
      new TH2F("hP1Vst0", "P_{1} Vs t", 100, -8,8, 100, 0, 1),
      new TH2F("hP1Vst1", "P_{1} Vs t", 100, -8,8, 100, 0, 1),
      new TH2F("hP1Vst2", "P_{1} Vs t", 100, -8,0, 100, 0, 1),
      new TH2F("hP1Vst3", "P_{1} Vs t", 100, -8,0, 100, 0, 1),
      new TH2F("hP1Vst4", "P_{1} Vs t", 100, -8,0, 100, 0, 1)
   };
   std::vector<TH2F*> hThetaP1Vst = {
      new TH2F("hThetaP1Vst0", "P_{1} Vs t", 100, -8, 8, 90, 0, 180),
      new TH2F("hThetaP1Vst1", "P_{1} Vs t", 100, -8, 8, 90, 0, 180),
      new TH2F("hThetaP1Vst2", "P_{1} Vs t", 100, -3, 0, 90, 0, 180),
      new TH2F("hThetaP1Vst3", "P_{1} Vs t", 100, -3, 0, 90, 0, 180),
      new TH2F("hThetaP1Vst4", "P_{1} Vs t", 100, -3, 0, 90, 0, 180)
   };
   std::vector<TH1F*> hE_e = {
      new TH1F("hE0_e", "E", 100, 0, 11),
      new TH1F("hE1_e", "E", 100, 0, 11)
   };
   std::vector<TH1F*> hE_He4 = {
      new TH1F("hE_He40", "E", 100, 0, 11),
      new TH1F("hE_He41", "E", 100, 0, 11)
   };
   std::vector<TH1F*> hE_p = {
      new TH1F("hE0_p", "E proton", 100, 0, 11.0),
      new TH1F("hE1_p", "E proton", 100, 0, 11.0)
   };
   std::vector<TH1F*> hE_g = {
      new TH1F("hE0_g", "E", 100, 0, 11),
      new TH1F("hE1_g", "E", 100, 0, 11)
   };

   std::vector<TH1F*> hTheta_e = {
      new TH1F("hTheta0_e", "#theta", 90, 0, 180),
      new TH1F("hTheta1_e", "#theta", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_He4 = {
      new TH1F("hTheta0_He4", "#theta", 90, 0, 180),
      new TH1F("hTheta1_He4", "#theta", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_p = {
      new TH1F("hTheta0_p", "Theta proton", 90, 0, 180),
      new TH1F("hTheta1_p", "Theta proton", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_g = {
      new TH1F("hTheta0_g", "Theta", 90, 0, 180),
      new TH1F("hTheta1_g", "Theta", 90, 0, 180)
   };

   std::vector<TH1F*> hQ2 = {
      new TH1F("hQ20", "Q^{2}", 100, 0, 12),
      new TH1F("hQ21", "Q^{2}", 100, 0, 12),
      new TH1F("hQ22", "Q^{2}", 100, -0.2, 0.2)
   };
   std::vector<TH1F*> hx = {
      new TH1F("hx0", "x", 100, 0, 1),
      new TH1F("hx1", "x", 100, 0, 1),
      new TH1F("hx2", "x", 100, -0.5, 0.5),
      new TH1F("hx3", "x", 100, 0, 1),
      new TH1F("hx4", "x", 100, 0, 1),
      new TH1F("hx5", "x", 100, -0.5, 0.5),
      new TH1F("hx6", "x", 100, -0.5, 0.5)
   };
   std::vector<TH1F*> hW = {
      new TH1F("hW0", "W", 100, 0.5, 4),
      new TH1F("hW1", "W", 100, 0.5, 4),
      new TH1F("hW2", "W", 100,-0.2, 0.2),
      new TH1F("hW3", "W", 100, 0.5, 4),
      new TH1F("hW4", "W", 100, 0.5, 4),
      new TH1F("hW5", "W", 100,-0.2, 0.2),
      new TH1F("hW6", "W", 100,-0.2, 0.2)
   };
   std::vector<TH1F*> ht = {
      new TH1F("ht0", "t", 100, -4.0, 0.0),
      new TH1F("ht1", "t", 100, -4.0, 0.0),
      new TH1F("ht2", "t", 100, -1.0, 1.0),
      new TH1F("ht3", "t", 100, -4.0, 1.0),
      new TH1F("ht4", "t", 100, -4.0, 1.0),
      new TH1F("ht5", "t", 100, -1.0, 1.0),
      new TH1F("ht6", "t", 100, -1.0, 1.0),
      new TH1F("ht7", "t", 100, -4.0, 0.0),
      new TH1F("ht8", "t", 100, -4.0, 0.0),
      new TH1F("ht9", "t",  100, -2.0, 2.0),
      new TH1F("ht10", "t", 100, -2.0, 2.0)
   };
   std::vector<TH2F*> hDeltatVst = {
      new TH2F("hDeltatVst0", "#deltat Vs t", 100, -5, 5, 100, -5, 5),
      new TH2F("hDeltatVst1", "#deltat Vs t", 100, -5, 5, 100, -5, 5),
      new TH2F("hDeltatVst2", "exact t Vs approximate t", 100, -5, 0, 100, -5, 5),
      new TH2F("hDeltatVst3", "#deltat Vs t", 100, -5, 5, 100, -5, 5),
      new TH2F("hDeltatVst4", "#deltat Vs t", 100, -5, 5, 100, -5, 5)
   };
   std::vector<TH1F*> hE0 = {
      new TH1F("hE00", "E_{#gamma}", 100, 0.0, 4),
      new TH1F("hE01", "E_{#gamma}", 100, 0.0, 4)
   };
   std::vector<TH1F*> hThetaS = {
      new TH1F("hThetaS0", "#theta_{s}",  90, 0, 180),
      new TH1F("hThetaS1", "#theta_{s}",  90, 0, 180),
      new TH1F("hThetaS2", "#theta_{s}",  90, 0, 180),
      new TH1F("hThetaS3", "#theta_{s}",  90, 0, 180),
      new TH1F("hThetaS4", "#theta_{s}",  90, 0, 180),
      new TH1F("hThetaS5", "#theta_{s}",  90, 0, 180),
      new TH1F("hThetaS6", "#delta#theta_{s}",  50, -5, 5)
   };
   std::vector<TH2F*> hQ2Vsx = {
      new TH2F("hQ2Vsx0", "Q^{2} vs x", 40, 0, 1, 40, 0, 5),
      new TH2F("hQ2Vsx1", "Q^{2} vs x", 40, 0, 1, 40, 0, 5),
      new TH2F("hQ2Vsx2", "Q^{2} vs x", 40, 0, 1, 40, 0, 5),
      new TH2F("hQ2Vsx3", "Q^{2} vs x", 40, 0, 1, 40, 0, 5),
      new TH2F("hQ2Vsx4", "Q^{2} vs x", 40, 0, 1, 40, 0, 5)
   };

   std::vector<TH2F*> hQ2Vst = {
      new TH2F("hQ2Vst0", "Q^{2} vs t", 40, -4, 0, 40, 0, 5),
      new TH2F("hQ2Vst1", "Q^{2} vs t", 40, -4, 0, 40, 0, 5),
      new TH2F("hQ2Vst2", "Q^{2} vs t", 40, -4, 0, 40, 0, 5),
      new TH2F("hQ2Vst3", "Q^{2} vs t", 40, -4, 0, 40, 0, 5),
      new TH2F("hQ2Vst4", "Q^{2} vs t", 40, -4, 0, 40, 0, 5)
   };
   std::vector<TH2F*> hP1VsThetas = {
      new TH2F("hP1VsThetas0", "P1 vs #theta_{s}", 90, 0, 180, 100, 0, 1),
      new TH2F("hP1VsThetas1", "P1 vs #theta_{s}", 90, 0, 180, 100, 0, 1),
      new TH2F("hP1VsThetas2", "P1 vs #theta_{s}", 90, 0, 180, 100, 0, 1),
      new TH2F("hP1VsThetas3", "P1 vs #theta_{s}", 90, 0, 180, 100, 0, 1),
      new TH2F("hP1VsThetas4", "P1 vs #theta_{s}", 90, 0, 180, 100, 0, 1)
   };

   // ----------------------------------------------------------------------
   // Event loop 
   int nevents = t->GetEntries();
   int n_accepted_total = 0;
   std::cout << nevents << " Events." << std::endl;
   for(int ievent = 0; ievent < nevents ; ievent++){

      t->GetEntry(ievent);
      if(ievent%10000==0) {
         std::cout << ievent << std::endl;
      }

      int n_thrown  = thrown->fNParticles;

      int helicity = int(thrown->fBeamPol);

      int status = 0;

      bool good_proton  = false;
      bool good_gamma   = false;
      bool good_e       = false;
      bool good_triton  = false;


      TLorentzVector  p_tot = {0,0,0.0,0.0};
      // ------------------------------------------------------------------
      // 
      for(int ithrown = 0; ithrown<n_thrown; ithrown++){

         auto p0 = thrown->GetParticle( ithrown);
         auto p1 = smeared->GetParticle(ithrown);
         //std::cout << p0->GetPdgCode() << std::endl;;

         auto hm = event->TrackHitMask(ithrown);
         if(!hm) continue;
         //if( hm->fDC < 18 ) continue;

         TLorentzVector  mom ;
         p0->Momentum( mom );

         if( ithrown != 3 ){
            p_tot += mom;
         }

         bool good_dc  = false;
         bool good_rc  = false;
         bool good_rh  = false;
         bool good_all = false;

         // hm->fDC
         if( hm->fDC >30) {
            good_dc = true;
         }
         if( hm->fRC >7) {
            good_rc = true;
         }
         if( hm->fRH >0) {
            good_rh = true;
         }
         if( good_dc /*&& good_rh && good_rc*/ ){
            good_all = true;
         }


         if( p0->GetPdgCode() == 11 ) {

            hE_e       [0]->Fill( p0->Energy() );
            hTheta_e   [0]->Fill( p0->Theta()/degree );

            if(good_dc){

               status++;
               good_e = true;

               hE_e       [1]->Fill(p1->Energy() );
               hTheta_e   [1]->Fill(p1->Theta()/degree );

               hThetaP    [0]->Fill(p1->P(), p1->Theta()/degree);
               hThetaPhi  [0]->Fill(p1->Phi()/degree, p0->Theta()/degree);
               hPhi       [0]->Fill(p1->Phi()/degree);

               hDeltaE    [0]->Fill(p0->P()- p1->P());
               hDeltaTheta[0]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }

         } else if( p0->GetPdgCode() == 2212 ) {

            hE_p    [0]->Fill( p0->Energy() );
            hTheta_p[0]->Fill( p0->Theta()/degree );

            if(good_dc){

               status++;
               good_proton = true;

               hE_p       [1]->Fill( p1->Energy() );
               hTheta_p   [1]->Fill( p1->Theta()/degree );

               hThetaP    [1]->Fill(p1->P(), p1->Theta()/degree);
               hThetaPhi  [1]->Fill(p1->Phi()/degree,p0->Theta()/degree);
               hPhi       [1]->Fill(p1->Phi()/degree);

               hDeltaE    [1]->Fill(p0->P() - p1->P());
               hDeltaTheta[1]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }
         } else if( p0->GetPdgCode() == 22 ) {

            hE_g[0]->Fill( p0->Energy() );
            hTheta_g[0]->Fill( p0->Theta()/degree );

            if(good_dc){

               status++;
               good_gamma = true;

               hE_g    [1]->Fill( p1->Energy() );
               hTheta_g[1]->Fill( p1->Theta()/degree );

               hThetaP  [2]->Fill(p1->P(), p1->Theta()/degree);
               hThetaPhi[2]->Fill(p1->Phi()/degree, p1->Theta()/degree);
               hPhi     [2]->Fill(p1->Phi()/degree);

               hDeltaE    [2]->Fill(p0->P()- p1->P());
               hDeltaTheta[2]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }

         } else if( (p0->GetPdgCode() == 1000010030) ) {

            hE_He4    [0]->Fill( p0->P() );
            hTheta_He4[0]->Fill( p0->Theta()/degree );

            if(good_rh && good_rc){

               status++;
               good_triton = true;

               hE_He4    [1]->Fill( p1->P() );
               hTheta_He4[1]->Fill( p1->Theta()/degree );

               hThetaP  [3]->Fill(p1->P(), p1->Theta()/degree);
               hThetaPhi[3]->Fill(p1->Phi()/degree, p1->Theta()/degree);
               hPhi     [3]->Fill(p1->Phi()/degree);

               hDeltaE    [3]->Fill(p0->P()- p1->P());
               hDeltaTheta[3]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }
         }

      }

      bool good_incoherent_event = (good_e && good_gamma && good_triton && good_proton );

      // -------------------------------------------------
      //
      if( good_incoherent_event && (status > 3) ) {

         double t_thrown  = eKine0->t_true();
         double W_thrown  = eKine0->W();
         double Q2_thrown = eKine0->Q2();
         double x_thrown  = eKine0->x();

         double t_recon  = eKine->t_true();
         double W_recon  = eKine->W();
         double Q2_recon = eKine->Q2();
         double x_recon  = eKine->x();

         double t_gammas_thrown  = eKine0->t_q1q2();
         double t_gammas_recon   = eKine->t_q1q2();
         double delta_t_gammas = (t_gammas_thrown-t_gammas_recon)/t_gammas_thrown;

         double delta_x  = (x_thrown  - x_recon )/x_thrown;
         double delta_t  = (t_thrown  - t_recon )/t_thrown;
         double delta_W  = (W_thrown  - W_recon )/W_thrown;
         double delta_Q2 = (Q2_thrown - Q2_recon)/Q2_thrown;

         double P1_thrown = eKine0->p1().Vect().Mag();
         double P1_recon  = eKine->p1().Vect().Mag();

         double P1_theta_thrown = eKine0->p1().Vect().Theta();
         double P1_theta_recon  = eKine->p1().Vect().Theta();

         double x_exact_thrown  = eKine0->x_true();
         double x_exact_recon   = eKine->x_true();
         double x_approx_thrown = eKine0->x(); 
         double x_approx_recon  = eKine->x(); 

         double t_exact_thrown  = eKine0->t_true();
         double t_exact_recon   = eKine->t_true();
         double t_approx_thrown = eKine0->t_approx();
         double t_approx_recon  = eKine->t_approx();

         double delta_x_exact  = (x_exact_thrown  - x_exact_recon )/x_exact_thrown;
         double delta_x_approx = (x_approx_thrown - x_approx_recon)/x_approx_thrown;
         double x_diff_thrown  = (x_exact_thrown - x_approx_thrown)/x_exact_thrown;
         double x_diff_recon   = (x_exact_recon  - x_approx_recon)/x_exact_recon;

         double delta_t_exact  = (t_exact_thrown  - t_exact_recon )/t_exact_thrown;
         double delta_t_approx = (t_approx_thrown - t_approx_recon)/t_approx_thrown;
         double t_diff_thrown  = (t_exact_thrown - t_approx_thrown)/t_exact_thrown;
         double t_diff_recon   = (t_exact_recon - t_approx_recon)/t_exact_recon;

         double t1_minus_t2_recon    = (t_exact_recon-t_gammas_recon);

         double W_val  = eKine0->W();
         double Q2_val = eKine0->Q2();

         hx    [0]->Fill( x_exact_thrown );
         hQ2   [0]->Fill( eKine0->Q2()   );
         hW    [0]->Fill( eKine0->W()    );
         ht    [0]->Fill( t_exact_thrown );
         hP1Vsx[0]->Fill( x_exact_thrown ,P1_thrown);

         hx    [1]->Fill( x_exact_recon );
         hQ2   [1]->Fill( eKine->Q2()   );
         hW    [1]->Fill( eKine->W()    );
         ht    [1]->Fill( t_exact_recon );
         hP1Vsx[1]->Fill( x_exact_recon ,P1_recon);

         hx [2]->Fill( delta_x_exact );
         hQ2[2]->Fill( (eKine0->Q2()-eKine->Q2())/eKine0->Q2() );
         hW [2]->Fill( (eKine0->W() - eKine->W())/eKine0->W()  );
         ht [2]->Fill( delta_t_exact );

         hP1Vsx[2]->Fill(delta_x_exact, (P1_thrown - P1_recon)/P1_thrown );
         hP1Vsx[3]->Fill(x_approx_thrown ,P1_thrown);
         hP1Vsx[4]->Fill(x_approx_recon , P1_recon);

         ht[3]->Fill(t_approx_thrown);
         ht[4]->Fill(t_approx_recon);
         ht[5]->Fill(delta_t_approx);
         ht[6]->Fill(delta_t_gammas);
         ht[7]->Fill(t_gammas_thrown);
         ht[8]->Fill(t_gammas_recon);
         //ht[7]->Fill(t_diff_thrown);
         
         hDeltatVst[0]->Fill(t_exact_recon , t_diff_recon);
         hDeltatVst[1]->Fill(t_approx_recon, t_diff_recon);
         hDeltatVst[2]->Fill(t_exact_recon, t_approx_recon);

         hx[3]->Fill(x_approx_thrown);
         hx[4]->Fill(x_approx_recon);
         hx[5]->Fill(delta_x_approx);
         hx[6]->Fill(x_diff_recon);

         hP1Vst[0]->Fill(t_diff_thrown, P1_thrown);
         hP1Vst[1]->Fill(t_diff_recon , P1_recon);
         hP1Vst[2]->Fill(t_exact_recon , P1_recon);
         hP1Vst[3]->Fill(t_approx_recon, P1_recon);
         //hP1Vst[4]->Fill(t_exact_recon , P1_recon);

         hThetaP1Vst[0]->Fill(t_diff_thrown, P1_theta_thrown/degree);
         hThetaP1Vst[1]->Fill(t_diff_recon,  P1_theta_recon/degree);
         hThetaP1Vst[2]->Fill(t_exact_recon ,  P1_theta_recon/degree);
         hThetaP1Vst[3]->Fill(t_approx_recon,  P1_theta_recon/degree);
         //hThetaP1Vst[4]->Fill(t_exact_recon ,  P1_theta_recon/degree);

         TVector3 q1  = eKine0->q().Vect();
         TVector3 ps  = eKine0->p1().Vect();
         double theta_s_thrown = q1.Angle(ps)/degree;
         hThetaS[0]->Fill(theta_s_thrown);
         q1  = eKine->q().Vect();
         ps  = eKine->p1().Vect();
         double theta_s_recon = q1.Angle(ps)/degree;
         double delta_theta_s = (theta_s_thrown-theta_s_recon)/theta_s_thrown;
         hThetaS[1]->Fill(theta_s_recon);
         hThetaS[2]->Fill(ps.Theta()/degree);
         if(W_val > 2.0 ) {
            hThetaS[3]->Fill(theta_s_recon);
         }
         if(Q2_val > 1.0 ) {
            hThetaS[4]->Fill(theta_s_recon);
         }
         hThetaS[6]->Fill(delta_theta_s);
         if(helicity == 0) {
            hPhiAsym[0]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsym[1]->Fill(eKine0->phi_dvcs()/degree);
         }else if(helicity == 1) {
            hPhiAsym[0]->Fill(eKine0->phi_dvcs()/degree);
         }else if(helicity == -1) {
            hPhiAsym[1]->Fill(eKine0->phi_dvcs()/degree);
         }

         int i_Q2_bin = 4;
         int i_t_bin  = 4;
         int i_x_bin  = 4;
         int i_P1_bin  = 4;
         int i_thetaP1_bin  = 0;
         for(int ibin = 0; ibin<4; ibin++) {
            if((Q2bins[ibin] <= Q2_recon) && (Q2bins[ibin+1] > Q2_recon) ) {
               i_Q2_bin=ibin;
            }
            if((P1bins[ibin] <= P1_recon) && (P1bins[ibin+1] > P1_recon) ) {
               i_P1_bin=ibin;
            }
            if((xbins[ibin] <= x_recon) && (xbins[ibin+1] > x_recon) ) {
               i_x_bin=ibin;
            }
            if((tbins[ibin] <= TMath::Abs(t_recon)) && (tbins[ibin+1] > t_recon) ) {
               i_t_bin=ibin;
            }
            //std::cout << tbins[ibin] << " <  " << TMath::Abs(t_recon) <<std::endl;
         }
         for(int ibin = 0; ibin<3; ibin++) {
            if((thetaP1bins[ibin] <= P1_theta_recon/degree) && (thetaP1bins[ibin+1] > P1_theta_recon/degree) ) {
               i_thetaP1_bin = ibin;
            }
         }

         if(helicity == 0) {
            hPhiAsymBinned[i_x_bin][i_Q2_bin][i_t_bin][0]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinned[i_x_bin][i_Q2_bin][i_t_bin][1]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinnedP1[i_x_bin][i_Q2_bin][i_t_bin][i_P1_bin][0]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinnedP1[i_x_bin][i_Q2_bin][i_t_bin][i_P1_bin][1]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinnedP1ThetaP1[i_x_bin][i_Q2_bin][i_t_bin][i_P1_bin][i_thetaP1_bin][0]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinnedP1ThetaP1[i_x_bin][i_Q2_bin][i_t_bin][i_P1_bin][i_thetaP1_bin][1]->Fill(eKine0->phi_dvcs()/degree);
         }else if(helicity == 1) {
            //std::cout << i_x_bin << i_Q2_bin << i_t_bin << std::endl;
            hPhiAsymBinned[i_x_bin][i_Q2_bin][i_t_bin][0]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinnedP1[i_x_bin][i_Q2_bin][i_t_bin][i_P1_bin][0]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinnedP1ThetaP1[i_x_bin][i_Q2_bin][i_t_bin][i_P1_bin][i_thetaP1_bin][0]->Fill(eKine0->phi_dvcs()/degree);
         }else if(helicity == -1) {
            hPhiAsymBinned[i_x_bin][i_Q2_bin][i_t_bin][1]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinnedP1[i_x_bin][i_Q2_bin][i_t_bin][i_P1_bin][1]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsymBinnedP1ThetaP1[i_x_bin][i_Q2_bin][i_t_bin][i_P1_bin][i_thetaP1_bin][1]->Fill(eKine0->phi_dvcs()/degree);
         }

         hQ2Vsx[0]->Fill(x_exact_recon, eKine->Q2() );
         hQ2Vst[0]->Fill(t_exact_recon, eKine->Q2() );
         hP1VsThetas[0]->Fill(theta_s_recon, P1_recon );
         ht[9]->Fill(t1_minus_t2_recon);

         // -------------------------------------------------
         // Cuts
         if( (W_recon > 2.0) && (Q2_recon > 1.0) ) {

            hQ2Vsx[1]->Fill(x_exact_recon, eKine->Q2() );
            hQ2Vst[1]->Fill(t_exact_recon, eKine->Q2() );
            hP1VsThetas[1]->Fill(theta_s_recon, P1_recon );

            ht[10]->Fill(t1_minus_t2_recon);

            hThetaPhi[4]->Fill(eKine->k2().Phi()/degree, eKine->k2().Theta()/degree);
            hThetaPhi[5]->Fill(eKine->p2().Phi()/degree, eKine->p2().Theta()/degree);
            hThetaPhi[6]->Fill(eKine->p0().Phi()/degree, eKine->p0().Theta()/degree);
            hThetaPhi[7]->Fill(eKine->p1().Phi()/degree, eKine->p1().Theta()/degree);

            hThetaP[4]->Fill(eKine->k2().Vect().Mag(), eKine->k2().Theta()/degree);
            hThetaP[5]->Fill(eKine->p2().Vect().Mag(), eKine->p2().Theta()/degree);
            hThetaP[6]->Fill(eKine->p0().Vect().Mag(), eKine->p0().Theta()/degree);
            hThetaP[7]->Fill(eKine->p1().Vect().Mag(), eKine->p1().Theta()/degree);


            //hE_He4[2]->Fill( eKine0->p2().Vect().Mag() );
            //hE_g  [2]->Fill( eKine0->p0().Vect().Mag() );
            //hE_e  [2]->Fill( eKine0->k2().Vect().Mag() );

            //hTheta_He4[2]->Fill( eKine0->p2().Vect().Theta()/degree);
            //hTheta_g  [2]->Fill( eKine0->p0().Vect().Theta()/degree);
            //hTheta_e  [2]->Fill( eKine0->k2().Vect().Theta()/degree);

            //ht[3]->Fill(t_thrown);
            //ht[4]->Fill(t_recon);
            //ht[5]->Fill(delta_t);

            //hx[3]->Fill(x_thrown);
            //hx[4]->Fill(x_recon);
            //hx[5]->Fill(delta_x);

            //hW[3]->Fill(W_thrown);
            //hW[4]->Fill(W_recon);
            //hW[5]->Fill(delta_W);

            //hQ2[3]->Fill(Q2_thrown);
            //hQ2[4]->Fill(Q2_recon);
            //hQ2[5]->Fill(delta_Q2);

            //double weight = 86400;
            if(helicity == 0) {
               hPhiAsym[2]->Fill(eKine0->phi_dvcs()/degree);
               hPhiAsym[3]->Fill(eKine0->phi_dvcs()/degree);
            }else if(helicity == 1) {
               hPhiAsym[2]->Fill(eKine0->phi_dvcs()/degree);
            }else if(helicity == -1) {
               hPhiAsym[3]->Fill(eKine0->phi_dvcs()/degree);
            }
            n_accepted_total++;
         }

      }
   }
   fout->Write();

   // ----------------------------------------------------------------------
   // 
   TCanvas * c   = 0;
   TLegend * leg = 0;
   THStack * hs  = 0;
   std::vector<int> colors = {1,2,4,6,7,3,8,9,30,34,37,41,46};

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.3, 0.7, 0.7, 0.9);
   hs  = new THStack("energies","P");
   gPad->SetLogy(true);
   for(int i = 0; i<2; i++) {
      hE_e[i]->SetLineWidth(2);
      hE_p[i]->SetLineWidth(2);
      hE_g[i]->SetLineWidth(2);
      hE_He4[i]->SetLineWidth(2);

      hE_e[i]->SetLineColor(colors[i]);
      hE_p[i]->SetLineColor(colors[i+2]);
      hE_g[i]->SetLineColor(colors[i+4]);
      hE_He4[i]->SetLineColor(colors[i+6]);

      hs->Add(hE_e[i]);
      hs->Add(hE_p[i]);
      hs->Add(hE_g[i]);
      hs->Add(hE_He4[i]);
   }
   leg->SetNColumns(2);
   leg->AddEntry(hE_e[0], "e- thrown",     "l");
   leg->AddEntry(hE_e[1], "e- smeared",     "l");
   leg->AddEntry(hE_p[0], "p thrown",      "l");
   leg->AddEntry(hE_p[1], "p  smeared",      "l");
   leg->AddEntry(hE_g[0], "#gamma thrown", "l");
   leg->AddEntry(hE_g[1], "#gamma smeared", "l");
   leg->AddEntry(hE_He4[0], "recoil thrown", "l");
   leg->AddEntry(hE_He4[1], "recoil smeared", "l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("P [GeV/c]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_0.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_0.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.45, 0.72, 0.85, 0.9);
   hs  = new THStack("theta_hists","#theta");
   gPad->SetLogy(true);
   for(int i = 0; i<2; i++) {
      hTheta_e[i]->SetLineWidth(2);
      hTheta_p[i]->SetLineWidth(2);
      hTheta_g[i]->SetLineWidth(2);
      hTheta_He4[i]->SetLineWidth(2);

      hTheta_e[i]->SetLineColor(colors[i]);
      hTheta_p[i]->SetLineColor(colors[i+2]);
      hTheta_g[i]->SetLineColor(colors[i+4]);
      hTheta_He4[i]->SetLineColor(colors[i+6]);

      hs->Add(hTheta_e[i]);
      hs->Add(hTheta_p[i]);
      hs->Add(hTheta_g[i]);
      hs->Add(hTheta_He4[i]);
   }
   leg->SetNColumns(2);
   leg->AddEntry(hTheta_e[0], "e- thrown",     "l");
   leg->AddEntry(hTheta_e[1], "e- smeared",     "l");
   leg->AddEntry(hTheta_p[0], "p thrown",      "l");
   leg->AddEntry(hTheta_p[1], "p  smeared",      "l");
   leg->AddEntry(hTheta_g[0], "#gamma thrown", "l");
   leg->AddEntry(hTheta_g[1], "#gamma smeared", "l");
   leg->AddEntry(hTheta_He4[0], "recoil thrown", "l");
   leg->AddEntry(hTheta_He4[1], "recoil smeared", "l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta [deg]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_1.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_1.pdf",dirname.c_str(),runnumber));

   int N_th =hTheta_He4[0]->GetEntries();
   int N_ac =hTheta_He4[1]->GetEntries();
   std::cout << "thrown/accepted = " <<  n_accepted_total << "/"  << nevents << " = " << double(n_accepted_total)/double(nevents) <<std::endl;
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("Whists","W");

   for(int i = 0; i<2; i++) {
      hW[i]->SetLineColor(colors[i]);
      hs->Add(hW[i]);
   }
   leg->AddEntry(hW[0],"thrown");
   leg->AddEntry(hW[1],"recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("W [GeV]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_2.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_2.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("xhists","x");

   for(int i = 0; i<2; i++) {
      hx[i]->SetLineColor(colors[i]);
      hs->Add(hx[i]);
   }
   for(int i = 3; i<5; i++) {
      hx[i]->SetLineColor(colors[i]);
      hs->Add(hx[i]);
   }
   leg->AddEntry(hx[0],"exact thrown");
   leg->AddEntry(hx[1],"exact recon");
   leg->AddEntry(hx[3],"approx thrown");
   leg->AddEntry(hx[4],"approx recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("x_{B}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_3.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_3.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("Q2hists","Q^{2}");

   for(int i = 0; i<2; i++) {
      hQ2[i]->SetLineColor(colors[i]);
      hs->Add(hQ2[i]);
   }
   leg->AddEntry(hQ2[0],"thrown");
   leg->AddEntry(hQ2[1],"recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_4.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_4.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.2, 0.7, 0.4, 0.9);
   hs  = new THStack("thists","t");
   ht[0]->SetLineColor(1);
   ht[1]->SetLineColor(2);
   ht[7]->SetLineColor(4);
   ht[7]->SetLineStyle(2);
   ht[8]->SetLineColor(4);
   hs->Add(ht[0]);
   hs->Add(ht[1]);
   hs->Add(ht[7]);
   hs->Add(ht[8]);
   leg->AddEntry(ht[0],"thrown");
   leg->AddEntry(ht[1],"recon");
   leg->AddEntry(ht[7],"photons thrown");
   leg->AddEntry(ht[8],"photons recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("t [GeV^{2}]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_5.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_5.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaEhists","#Delta P");

   for(int i = 0; i<3; i++) {
      hDeltaE[i]->SetLineColor(colors[i]);
      hs->Add(hDeltaE[i]);
   }
   leg->AddEntry(hDeltaE[0],"electrons");
   leg->AddEntry(hDeltaE[2],"photons");
   leg->Draw();
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("P_{thrown} - P_{recon} [GeV]");
   hs->GetYaxis()->SetTitle("");
   c->SaveAs(Form("%s/He4_proton_%d_6.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_6.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaTHetahists","#Delta #theta");
   for(int i = 0; i<3; i++) {
      hDeltaTheta[i]->SetLineColor(colors[i]);
      hs->Add(hDeltaTheta[i]);
   }
   hs->Draw("nostack");
   hs->Draw("colz");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta_{thrown} - #theta_{recon} [deg]");
   hs->GetYaxis()->SetTitle("");
   c->SaveAs(Form("%s/He4_proton_%d_7.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_7.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   // Theta vs phi
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[0]->Draw("colz");
   hThetaPhi[0]->GetXaxis()->CenterTitle(true);
   hThetaPhi[0]->GetYaxis()->CenterTitle(true);
   hThetaPhi[0]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[0]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_8.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_8.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[1]->Draw("colz");
   hThetaPhi[1]->GetXaxis()->CenterTitle(true);
   hThetaPhi[1]->GetYaxis()->CenterTitle(true);
   hThetaPhi[1]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[1]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_81.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_81.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[2]->Draw("colz");
   hThetaPhi[2]->GetXaxis()->CenterTitle(true);
   hThetaPhi[2]->GetYaxis()->CenterTitle(true);
   hThetaPhi[2]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[2]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_82.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_82.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[3]->Draw("colz");
   hThetaPhi[3]->GetXaxis()->CenterTitle(true);
   hThetaPhi[3]->GetYaxis()->CenterTitle(true);
   hThetaPhi[3]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[3]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_83.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_83.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[4]->Draw("colz");
   hThetaPhi[4]->GetXaxis()->CenterTitle(true);
   hThetaPhi[4]->GetYaxis()->CenterTitle(true);
   hThetaPhi[4]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[4]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_84.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_84.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[5]->Draw("colz");
   hThetaPhi[5]->GetXaxis()->CenterTitle(true);
   hThetaPhi[5]->GetYaxis()->CenterTitle(true);
   hThetaPhi[5]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[5]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_85.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_85.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[6]->Draw("colz");
   hThetaPhi[6]->GetXaxis()->CenterTitle(true);
   hThetaPhi[6]->GetYaxis()->CenterTitle(true);
   hThetaPhi[6]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[6]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_86.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_86.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[7]->Draw("colz");
   hThetaPhi[7]->GetXaxis()->CenterTitle(true);
   hThetaPhi[7]->GetYaxis()->CenterTitle(true);
   hThetaPhi[7]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[7]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_87.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_87.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("phihists","#phi");

   for(int i = 0; i<3; i++) {
      hPhi[i]->SetLineColor(colors[i]);
      hs->Add(hPhi[i]);
   }
   hs->Draw("nostack");
   c->SaveAs(Form("%s/He4_proton_%d_9.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_9.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaXhists","#Delta x");
   hx[2]->SetLineColor(1);
   hx[5]->SetLineColor(2);
   hx[6]->SetLineColor(4);
   hs->Add(hx[2]);
   hs->Add(hx[5]);
   //hs->Add(hx[6]);
   leg->AddEntry(hx[2],"exact","l");
   leg->AddEntry(hx[5],"approx","l");
   //leg->AddEntry(hx[6],"(exact-approx)/exact","l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("(x_{thrown}-x_{recon})/x_{thrown}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_10.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_10.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.55, 0.7, 0.85, 0.9);
   hs  = new THStack("deltathists","#delta t");
   ht[9]->SetLineColor(1);
   //ht[10]->SetLineColor(2);
   hs->Add(ht[9]);
   leg->AddEntry(ht[9],"No cuts","l");
   //hs->Add(ht[5]);
   //hs->Add(ht[6]);
   //hs->Add(ht[10]);
   //leg->AddEntry(ht[9],"w/ Q^{2} and W cuts","l");
   //leg->AddEntry(ht[6],"t=(q_{1}-q_{2})^{2}","l");
   //leg->AddEntry(ht[5],"approx: #vec{p}_{1}=0","l");
   //leg->AddEntry(ht[6],"(exact-approx)/exact","l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("t_{p}-t_{q} [GeV^{2}]");
   hs->GetYaxis()->SetTitle("");
   //leg->Draw();
   c->SaveAs(Form("%s/He4_proton_deltat_%d_11.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_deltat_%d_11.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.55, 0.7, 0.85, 0.9);
   hs  = new THStack("deltathists","#Delta t");
   ht[2]->SetTitle("#sigma t");
   ht[2]->SetLineColor(1);
   //ht[5]->SetLineColor(2);
   ht[6]->SetLineColor(4);
   hs->Add(ht[2]);
   //hs->Add(ht[5]);
   hs->Add(ht[6]);
   leg->AddEntry(ht[2],"t=(p_{1}-p_{2})^{2}","l");
   leg->AddEntry(ht[6],"t=(q_{1}-q_{2})^{2}","l");
   //leg->AddEntry(ht[5],"approx: #vec{p}_{1}=0","l");
   //leg->AddEntry(ht[6],"(exact-approx)/exact","l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("(t_{thrown}-t_{recon})/t_{thrown}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_11.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_11.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.15, 0.6, 0.4, 0.9);
   hs  = new THStack("deltathists","#Delta t");
   ht[2]->SetLineColor(1);
   ht[2]->SetLineWidth(2);
   ht[6]->SetLineColor(4);
   ht[6]->SetLineWidth(2);
   hs->Add(ht[2]);
   hs->Add(ht[6]);
   leg->AddEntry(ht[2],"t=(p_{1}-p_{2})^{2}","l");
   leg->AddEntry(ht[6],"t=(q_{1}-q_{2})^{2}","l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("(t_{thrown} - t_{recon} )/t_{thrown}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_110.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_110.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hQ2[2]->SetTitle("#sigma Q^{2}");
   hQ2[2]->Draw();
   hQ2[2]->GetXaxis()->CenterTitle(true);
   hQ2[2]->GetYaxis()->CenterTitle(true);
   hQ2[2]->GetXaxis()->SetTitle("(Q^{2}_{thrown}-Q^{2}_{recon})/Q^{2}_{thrown}");
   hQ2[2]->GetYaxis()->SetTitle("");
   c->SaveAs(Form("%s/He4_proton_%d_12.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_12.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hW[2]->Draw();
   hW[2]->GetXaxis()->CenterTitle(true);
   hW[2]->GetYaxis()->CenterTitle(true);
   hW[2]->GetXaxis()->SetTitle("(W_{thrown}-W_{recon})/W_{thrown}");
   hW[2]->GetYaxis()->SetTitle("");
   c->SaveAs(Form("%s/He4_proton_%d_13.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_13.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[2]->Draw("colz");
   hP1Vsx[2]->GetXaxis()->CenterTitle(true);
   hP1Vsx[2]->GetYaxis()->CenterTitle(true);
   hP1Vsx[2]->GetXaxis()->SetTitle("(x_{thrown}-x_{recon})/x_{thrown}");
   hP1Vsx[2]->GetYaxis()->SetTitle("(P1_{thrown}-P1_{recon})/P1_{thrown}");
   c->SaveAs(Form("%s/He4_proton_%d_14.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_14.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[0]->Draw("colz");
   hP1Vsx[0]->GetXaxis()->CenterTitle(true);
   hP1Vsx[0]->GetYaxis()->CenterTitle(true);
   hP1Vsx[0]->GetXaxis()->SetTitle("x_{thrown}");
   hP1Vsx[0]->GetYaxis()->SetTitle("P1_{thrown}");
   c->SaveAs(Form("%s/He4_proton_%d_15.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_15.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[1]->Draw("colz");
   hP1Vsx[1]->GetXaxis()->CenterTitle(true);
   hP1Vsx[1]->GetYaxis()->CenterTitle(true);
   hP1Vsx[1]->GetXaxis()->SetTitle("x_{recon}");
   hP1Vsx[1]->GetYaxis()->SetTitle("P1_{recon}");
   c->SaveAs(Form("%s/He4_proton_%d_16.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_16.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[3]->Draw("colz");
   hP1Vsx[3]->GetXaxis()->CenterTitle(true);
   hP1Vsx[3]->GetYaxis()->CenterTitle(true);
   hP1Vsx[3]->GetXaxis()->SetTitle("x_{Tagged} = Q^{2}/2p_{1}q");
   hP1Vsx[3]->GetYaxis()->SetTitle("P1_{thrown}");
   c->SaveAs(Form("%s/He4_proton_%d_17.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_17.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[4]->Draw("colz");
   hP1Vsx[4]->GetXaxis()->CenterTitle(true);
   hP1Vsx[4]->GetYaxis()->CenterTitle(true);
   hP1Vsx[4]->GetXaxis()->SetTitle("x_{Tagged} = Q^{2}/2p_{1}q");
   hP1Vsx[4]->GetYaxis()->SetTitle("P1_{recon}");
   c->SaveAs(Form("%s/He4_proton_%d_18.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_18.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vst[1]->Draw("colz");
   hP1Vst[1]->GetXaxis()->CenterTitle(true);
   hP1Vst[1]->GetYaxis()->CenterTitle(true);
   hP1Vst[1]->GetXaxis()->SetTitle("(t_{exact}-t_{approx})/t_{exact}");
   hP1Vst[1]->GetYaxis()->SetTitle("P_{1} [GeV/c]");
   c->SaveAs(Form("%s/He4_proton_%d_19.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_19.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP1Vst[1]->Draw("colz");
   hThetaP1Vst[1]->GetXaxis()->CenterTitle(true);
   hThetaP1Vst[1]->GetYaxis()->CenterTitle(true);
   hThetaP1Vst[1]->GetXaxis()->SetTitle("(t_{exact}-t_{approx})/t_{exact}");
   hThetaP1Vst[1]->GetYaxis()->SetTitle("#theta_{P1} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_20.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_20.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vst[2]->Draw("colz");
   hP1Vst[2]->GetXaxis()->CenterTitle(true);
   hP1Vst[2]->GetYaxis()->CenterTitle(true);
   hP1Vst[2]->GetXaxis()->SetTitle("t_{recon}");
   hP1Vst[2]->GetYaxis()->SetTitle("P_{1} [GeV/c]");
   c->SaveAs(Form("%s/He4_proton_%d_21.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_21.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP1Vst[2]->Draw("colz");
   hThetaP1Vst[2]->GetXaxis()->CenterTitle(true);
   hThetaP1Vst[2]->GetYaxis()->CenterTitle(true);
   hThetaP1Vst[2]->GetXaxis()->SetTitle("t_{recon}");
   hThetaP1Vst[2]->GetYaxis()->SetTitle("#theta_{P1} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_22.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_22.pdf",dirname.c_str(),runnumber));
   
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vst[3]->Draw("colz");
   hP1Vst[3]->GetXaxis()->CenterTitle(true);
   hP1Vst[3]->GetYaxis()->CenterTitle(true);
   hP1Vst[3]->GetXaxis()->SetTitle("t_{recon/approx}");
   hP1Vst[3]->GetYaxis()->SetTitle("P_{1} [GeV/c]");
   c->SaveAs(Form("%s/He4_proton_%d_23.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_23.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP1Vst[3]->Draw("colz");
   hThetaP1Vst[3]->GetXaxis()->CenterTitle(true);
   hThetaP1Vst[3]->GetYaxis()->CenterTitle(true);
   hThetaP1Vst[3]->GetXaxis()->SetTitle("t_{recon/approx}");
   hThetaP1Vst[3]->GetYaxis()->SetTitle("#theta_{P1} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_24.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_24.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hDeltatVst[0]->Draw("colz");
   hDeltatVst[0]->GetXaxis()->CenterTitle(true);
   hDeltatVst[0]->GetYaxis()->CenterTitle(true);
   hDeltatVst[0]->GetXaxis()->SetTitle("t_{recon/exact}");
   hDeltatVst[0]->GetYaxis()->SetTitle("#Delta t (recon)");
   c->SaveAs(Form("%s/He4_proton_%d_25.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_25.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hDeltatVst[1]->Draw("colz");
   hDeltatVst[1]->GetXaxis()->CenterTitle(true);
   hDeltatVst[1]->GetYaxis()->CenterTitle(true);
   hDeltatVst[1]->GetXaxis()->SetTitle("t_{recon/approx}");
   hDeltatVst[1]->GetYaxis()->SetTitle("#Delta t (recon)");
   c->SaveAs(Form("%s/He4_proton_%d_26.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_26.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hDeltatVst[2]->Draw("colz");
   hDeltatVst[2]->GetXaxis()->CenterTitle(true);
   hDeltatVst[2]->GetYaxis()->CenterTitle(true);
   hDeltatVst[2]->GetXaxis()->SetTitle("t_{recon/exact}");
   hDeltatVst[2]->GetYaxis()->SetTitle("t_{recon/approx}");
   c->SaveAs(Form("%s/He4_proton_%d_27.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_27.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("thetaShists","Spectator System");
   hThetaS[0]->SetLineColor(1);
   hThetaS[1]->SetLineColor(2);
   //hThetaS[2]->SetLineColor(4);
   hThetaS[3]->SetLineColor(6);
   hThetaS[4]->SetLineColor(7);
   hs->Add(hThetaS[0]);
   hs->Add(hThetaS[1]);
   //hs->Add(hThetaS[2]);
   hs->Add(hThetaS[3]);
   hs->Add(hThetaS[4]);
   leg->AddEntry(hThetaS[0],"thrown","l");
   leg->AddEntry(hThetaS[1],"recon","l");
   //leg->AddEntry(hThetaS[2],"lab","l");
   leg->AddEntry(hThetaS[3],"W>2","l");
   leg->AddEntry(hThetaS[4],"Q2>1","l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta_{s}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_28.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_28.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaS[6]->SetLineColor(1);
   hThetaS[6]->Draw();
   hThetaS[6]->GetXaxis()->CenterTitle(true);
   hThetaS[6]->GetYaxis()->CenterTitle(true);
   hThetaS[6]->GetXaxis()->SetTitle("#Delta#theta_{s}");
   hThetaS[6]->GetYaxis()->SetTitle("");
   c->SaveAs(Form("%s/He4_proton_%d_280.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_280.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("asymHists","Asymmetry");

   //hPhiAsym[0]->Scale(86400);
   //hPhiAsym[1]->Scale(86400);
   TH1 * phi_asym = hPhiAsym[0]->GetAsymmetry(hPhiAsym[1]);
   for(int ibin = 1; ibin <= phi_asym->GetNbinsX(); ibin++) {
      phi_asym->SetBinContent(ibin,0.1);
   }
   phi_asym->SetLineColor(1);
   phi_asym->SetLineWidth(2);
   phi_asym->SetMarkerStyle(20);
   hs->Add(phi_asym);
   leg->AddEntry(phi_asym,"all","ep");
   //hPhiAsym[2]->Scale(86400);
   //hPhiAsym[3]->Scale(86400);
   phi_asym = hPhiAsym[2]->GetAsymmetry(hPhiAsym[3]);
   for(int ibin = 1; ibin <= phi_asym->GetNbinsX(); ibin++) {
      phi_asym->SetBinContent(ibin,0.2);
   }
   phi_asym->SetLineColor(2);
   phi_asym->SetLineWidth(2);
   phi_asym->SetMarkerStyle(20);
   phi_asym->SetMarkerColor(2);
   hs->Add(phi_asym);
   leg->AddEntry(phi_asym,"with cuts","ep");

   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#phi");
   hs->GetYaxis()->SetTitle("");
   hs->SetMinimum(-1);
   hs->SetMaximum(1);
   leg->Draw();
   c->SaveAs(Form("%s/He4_proton_%d_29.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_29.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   // Theta vs P
   c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[0]->Draw("colz");
   hThetaP[0]->GetXaxis()->CenterTitle(true);
   hThetaP[0]->GetYaxis()->CenterTitle(true);
   hThetaP[0]->GetXaxis()->SetTitle("P [GeV/c]");
   hThetaP[0]->GetYaxis()->SetTitle("#theta_{e} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_90.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_90.pdf",dirname.c_str(),runnumber));

   //// ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[1]->Draw("colz");
   hThetaP[1]->GetXaxis()->CenterTitle(true);
   hThetaP[1]->GetYaxis()->CenterTitle(true);
   hThetaP[1]->GetXaxis()->SetTitle("P [GeV/c]");
   hThetaP[1]->GetYaxis()->SetTitle("#theta_{p} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_91.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_91.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[2]->Draw("colz");
   hThetaP[2]->GetXaxis()->CenterTitle(true);
   hThetaP[2]->GetYaxis()->CenterTitle(true);
   hThetaP[2]->GetXaxis()->SetTitle("P [GeV/c]");
   hThetaP[2]->GetYaxis()->SetTitle("#theta_{#gamma} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_92.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_92.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   hThetaP[3]->Draw("colz");
   hThetaP[3]->GetXaxis()->CenterTitle(true);
   hThetaP[3]->GetYaxis()->CenterTitle(true);
   hThetaP[3]->GetXaxis()->SetTitle("P_{A-1} [GeV/c]");
   hThetaP[3]->GetYaxis()->SetTitle("#theta_{A-1} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_93.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_93.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[4]->Draw("colz");
   hThetaP[4]->GetXaxis()->CenterTitle(true);
   hThetaP[4]->GetYaxis()->CenterTitle(true);
   hThetaP[4]->GetXaxis()->SetTitle("P_{e} [GeV/c]");
   hThetaP[4]->GetYaxis()->SetTitle("#theta_{e} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_94.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_94.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[5]->Draw("colz");
   hThetaP[5]->GetXaxis()->CenterTitle(true);
   hThetaP[5]->GetYaxis()->CenterTitle(true);
   hThetaP[5]->GetXaxis()->SetTitle("P_{p} [GeV/c]");
   hThetaP[5]->GetYaxis()->SetTitle("#theta_{p} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_95.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_95.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[6]->Draw("colz");
   hThetaP[6]->GetXaxis()->CenterTitle(true);
   hThetaP[6]->GetYaxis()->CenterTitle(true);
   hThetaP[6]->GetXaxis()->SetTitle("P_{#gamma} [GeV/c]");
   hThetaP[6]->GetYaxis()->SetTitle("#theta_{#gamma} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_96.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_96.pdf",dirname.c_str(),runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[7]->Draw("colz");
   hThetaP[7]->GetXaxis()->CenterTitle(true);
   hThetaP[7]->GetYaxis()->CenterTitle(true);
   hThetaP[7]->GetXaxis()->SetTitle("P_{A-1} [GeV/c]");
   hThetaP[7]->GetYaxis()->SetTitle("#theta_{A-1} [deg]");
   c->SaveAs(Form("%s/He4_proton_%d_97.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_97.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   // x vs Q2
   c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hQ2Vsx[0]->Draw("colz");
   hQ2Vsx[0]->GetXaxis()->CenterTitle(true);
   hQ2Vsx[0]->GetYaxis()->CenterTitle(true);
   hQ2Vsx[0]->GetXaxis()->SetTitle("x");
   hQ2Vsx[0]->GetYaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");
   c->SaveAs(Form("%s/He4_proton_%d_70.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_70.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   hQ2Vsx[1]->Draw("colz");
   hQ2Vsx[1]->GetXaxis()->CenterTitle(true);
   hQ2Vsx[1]->GetYaxis()->CenterTitle(true);
   hQ2Vsx[1]->GetXaxis()->SetTitle("x");
   hQ2Vsx[1]->GetYaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");
   c->SaveAs(Form("%s/He4_proton_%d_71.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_71.pdf",dirname.c_str(),runnumber));


   // ---------------------------------------------
   hQ2Vst[0]->Draw("colz");
   hQ2Vst[0]->GetXaxis()->CenterTitle(true);
   hQ2Vst[0]->GetYaxis()->CenterTitle(true);
   hQ2Vst[0]->GetXaxis()->SetTitle("t [GeV^{2}]");
   hQ2Vst[0]->GetYaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");
   c->SaveAs(Form("%s/He4_proton_%d_72.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_72.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   hQ2Vst[1]->Draw("colz");
   hQ2Vst[1]->GetXaxis()->CenterTitle(true);
   hQ2Vst[1]->GetYaxis()->CenterTitle(true);
   hQ2Vst[1]->GetXaxis()->SetTitle("t [GeV^{2}]");
   hQ2Vst[1]->GetYaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");
   c->SaveAs(Form("%s/He4_proton_%d_73.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_73.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   hP1VsThetas[0]->Draw("colz");
   hP1VsThetas[0]->GetXaxis()->CenterTitle(true);
   hP1VsThetas[0]->GetYaxis()->CenterTitle(true);
   hP1VsThetas[0]->GetXaxis()->SetTitle("#theta_{s} [deg]");
   hP1VsThetas[0]->GetYaxis()->SetTitle("P_{A-1} [GeV/c]");
   c->SaveAs(Form("%s/He4_proton_%d_74.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_74.pdf",dirname.c_str(),runnumber));

   // ---------------------------------------------
   hP1VsThetas[1]->Draw("colz");
   hP1VsThetas[1]->GetXaxis()->CenterTitle(true);
   hP1VsThetas[1]->GetYaxis()->CenterTitle(true);
   hP1VsThetas[1]->GetXaxis()->SetTitle("#theta_{s} [deg]");
   hP1VsThetas[1]->GetYaxis()->SetTitle("P_{A-1} [GeV/c]");
   c->SaveAs(Form("%s/He4_proton_%d_75.png",dirname.c_str(),runnumber));
   c->SaveAs(Form("%s/He4_proton_%d_75.pdf",dirname.c_str(),runnumber));
   
   // ---------------------------------------------
   //
   // ---------------------------------------------
   using namespace InSANE::physics;

   // reference kinematics (rate normalization)
   double xB0    = 0.3;
   double Q20    = 1.5;
   double t0     = -1.0;
   double phi0   = 0*degree;
   double phi_e0 = 0*degree;
   double E0     = 11.0;

   IncoherentDVCSXSec * xsec0 = new IncoherentDVCSXSec();
   xsec0->SetBeamEnergy(E0);
   xsec0->SetHelicity( 1 );
   xsec0->SetTargetNucleus(InSANENucleus::He4());
   xsec0->SetRecoilNucleus( InSANENucleus::Proton()  );
   xsec0->InitializePhaseSpaceVariables();
   xsec0->InitializeFinalStateParticles();
   xsec0->UsePhaseSpace(false);

   IncoherentDVCSXSec * xsec1 = new IncoherentDVCSXSec();
   xsec1->SetBeamEnergy(E0);
   xsec1->SetHelicity( -1 );
   xsec1->SetTargetNucleus(InSANENucleus::He4());
   xsec1->SetRecoilNucleus( InSANENucleus::Proton()  );
   xsec1->InitializePhaseSpaceVariables();
   xsec1->InitializeFinalStateParticles();
   xsec1->UsePhaseSpace(false);

   IncoherentDVCSXSec * xsec6 = new IncoherentDVCSXSec();
   xsec6->SetBeamEnergy(11.0);
   xsec6->SetTargetNucleus( InSANENucleus::He4() );
   xsec6->SetRecoilNucleus( InSANENucleus::Proton()  );
   xsec6->InitializePhaseSpaceVariables();
   xsec6->InitializeFinalStateParticles();
   auto var = xsec6->GetPhaseSpace()->GetVariableWithName("energy_e");
   var->SetMinimum(0.5);
   var->SetMaximum(11.0);
   xsec6->UsePhaseSpace(false);
   TF1 * sigma6 = new TF1("sigma6", [&](double *x,double *p){
         using namespace TMath;
         double xB    = p[0];
         double Q2    = p[1];
         double t     = p[2];
         double p1    = p[3];
         double thetap1= p[4]*degree;
         double phi   = x[0]*degree;
         double phi_e = 0*degree;
         double vars[] = {phi_e0, xB0,t0,Q20,phi0,0.10,0.0*degree,0.0};
         double ref = xsec6->EvaluateXSec(xsec6->GetDependentVariables(vars));
         //std::cout << " ref = " << ref << std::endl;
         double vars2[] = {phi_e, xB,t,Q2,phi, p1, thetap1, 0.0};
         return( xsec6->EvaluateXSec(xsec6->GetDependentVariables(vars2))/ref );
         }, -180, 180, 3);
   sigma6->SetLineColor(2);
   xsec6->UsePhaseSpace(false);

   TF1 * sigma0 = new TF1("sigma0", [&](double *x,double *p){
         using namespace TMath;
         double xB    = p[0];
         double Q2    = p[1];
         double t     = p[2];
         double p1    = p[3];
         double thetap1= p[4]*degree;
         double phi   = x[0]*degree;
         double phi_e = 0*degree;
         double vars[] = {phi_e0, xB0,t0,Q20,phi0,0.10,0.0*degree,0.0};
         double ref = xsec0->EvaluateXSec(xsec0->GetDependentVariables(vars));
         //std::cout << " ref = " << ref << std::endl;
         double vars2[] = {phi_e, xB,t,Q2,phi,p1,thetap1,0.0};
         return( xsec0->EvaluateXSec(xsec0->GetDependentVariables(vars2))/ref );
         }, -180, 180, 5);
   sigma0->SetLineColor(2);
   xsec0->UsePhaseSpace(false);

   TF1 * sigma1 = new TF1("sigma1", [&](double *x,double *p){
         using namespace TMath;
         double xB    = p[0];
         double Q2    = p[1];
         double t     = p[2];
         double p1    = p[3];
         double thetap1= p[4]*degree;
         double phi   = x[0]*degree;
         double phi_e = 0*degree;
         double vars[] = {phi_e0, xB0,t0,Q20,phi0,0.10, 0.0*degree,0.0};
         double ref = xsec1->EvaluateXSec(xsec1->GetDependentVariables(vars));
         double vars2[] = {phi_e, xB,t,Q2,phi,p1, thetap1,0.0};
         return( xsec1->EvaluateXSec(xsec1->GetDependentVariables(vars2))/ref );
         }, -180, 180, 5);
   sigma1->SetLineColor(2);
   xsec1->UsePhaseSpace(false);

   int    refbin_0  = hQ2Vst[0]->FindBin(-1.0,1.5);
   double Nref_0    = hQ2Vst[0]->GetBinContent(refbin_0);
   int    refbin_1  = hQ2Vsx[0]->FindBin( 0.3,1.5);
   double Nref_1    = hQ2Vsx[0]->GetBinContent(refbin_1);

   std::cout << "Nref_0 = " << Nref_0 << std::endl;
   std::cout << "Nref_1 = " << Nref_1 << std::endl;

   //// ---------------------------------------------
   //c   = new TCanvas();
   ////c->Divide(4,4);
   //for(int i1=0;i1<4;i1++){
   //   for(int i2=0;i2<4;i2++){
   //      TMultiGraph * mg = new TMultiGraph();
   //      leg = new TLegend(0.8, 0.8, 0.99, 0.99);
   //      //c->cd(1+i1+i2*4);
   //      for(int i3=0;i3<4;i3++){
   //         TH1F * hist1 = hPhiAsymBinned.at(i1).at(i2).at(i3).at(0);
   //         TH1F * hist2 = hPhiAsymBinned.at(i1).at(i2).at(i3).at(1);
   //         //if(hist1->GetEntries()<5)continue;
   //         //if(hist2->GetEntries()<5)continue;

   //         double x_center  = (xbins[i1]+xbins[i1+1])/2.0;
   //         double Q2_center = Q2bins[i2];
   //         double t_center  = (tbins[i3]+tbins[i3+1])/2.0;
   //         //double P1_center = (P1bins[i4]+P1bins[i4+1])/2.0;
   //            //
   //         int    bin_0  = hQ2Vst[0]->FindBin(t_center, Q2_center);
   //         double N_0    = hQ2Vst[0]->GetBinContent(bin_0);
   //         int    bin_1  = hQ2Vsx[0]->FindBin(x_center,1.5);
   //         double N_1    = hQ2Vsx[0]->GetBinContent(bin_1);

   //         //std::cout << "x_center "  << x_center << std::endl;
   //         //std::cout << "Q2_center " << Q2_center << std::endl;
   //         //std::cout << "t_center "  << t_center << std::endl;
   //         sigma0->SetParameters( x_center, Q2_center, -1.0*t_center, 0.10, 0.0 );
   //         sigma1->SetParameters( x_center, Q2_center, -1.0*t_center, 0.10, 0.0 );
   //         sigma6->SetParameters( x_center, Q2_center, -1.0*t_center, 0.10, 0.0 );


   //         TH1  * rel_y1 = sigma0->DrawCopy("goff")->GetHistogram();
   //         TH1  * rel_y2 = sigma1->DrawCopy("goff")->GetHistogram();
   //         hist1->Reset();
   //         hist2->Reset();
   //         int n0 = 3e5;
   //         int n1 = int(n0*N_1/Nref_1);
   //         int n2 = int(n0*N_1/Nref_1);

   //         std::cout << "scale factor " <<  N_1/Nref_1 << std::endl;;

   //         hist1->FillRandom(rel_y1,n1);
   //         hist2->FillRandom(rel_y2,n2);

   //         TH1F * yield = (TH1F *)hist1->Clone("yield");
   //         yield->Add(hist2);
   //         yield->Sumw2(false);
   //         yield->Sumw2(true);

   //         yield->SetLineColor(1+i3);
   //         yield->SetLineWidth(2);
   //         yield->SetMarkerStyle(20);
   //         yield->SetMarkerColor(1+i3);
   //         yield->SetMarkerSize(0.6);
   //         TGraphErrors * gr = new TGraphErrors(yield);
   //         mg->Add(gr,"ep");
   //         leg->AddEntry(yield,Form("t=%.2f",(tbins[i3]+tbins[i3+1])/2.0),"ep");
   //      }
   //      mg->SetTitle(Form("x=%.2f, Q^{2}=%.2f",(xbins[i1]+xbins[i1+1])/2.0,(Q2bins[i2]+Q2bins[i2+1])/2.0));
   //      mg->Draw("a");
   //      //mg->GetYaxis()->SetRangeUser(-1.0,1.0);
   //      mg->GetYaxis()->CenterTitle(true);
   //      mg->GetXaxis()->CenterTitle(true);
   //      mg->GetYaxis()->SetTitle("yield");
   //      mg->GetXaxis()->SetTitle("#phi [deg]");
   //      mg->Draw("a");
   //      gPad->SetLogy(true);
   //      mg->Draw("a");
   //      leg->Draw();
   //      c->SaveAs(Form("%s/He4_proton_%d_yields_%d_%d.png",dirname.c_str(),runnumber,,i1,i2));
   //      c->SaveAs(Form("%s/He4_proton_%d_yields_%d_%d.pdf",dirname.c_str(),runnumber,,i1,i2));
   //   }
   //}
   //std::cout << "Nref_0 = " << Nref_0 << std::endl;
   //std::cout << "Nref_1 = " << Nref_1 << std::endl;
   //return ;
   
   // ---------------------------------------------
   //c   = new TCanvas();
   ////c->Divide(4,4);
   //for(int i1=0;i1<4;i1++){
   //   for(int i2=0;i2<4;i2++){
   //      //c->cd(1+i1+i2*4);
   //      for(int i3=0;i3<4;i3++){
   //         for(int i4=0;i4<4;i4++){
   //            TMultiGraph * mg = new TMultiGraph();
   //            leg = new TLegend(0.8, 0.8, 0.99, 0.99);
   //            for(int i5=0;i5<3;i5++){
   //               TH1F * hist1 = hPhiAsymBinnedP1ThetaP1.at(i1).at(i2).at(i3).at(i4).at(i5).at(0);
   //               TH1F * hist2 = hPhiAsymBinnedP1ThetaP1.at(i1).at(i2).at(i3).at(i4).at(i5).at(1);
   //               //if(hist1->GetEntries()<5)continue;
   //               //if(hist2->GetEntries()<5)continue;

   //               double x_center  = (xbins[i1]+xbins[i1+1])/2.0;
   //               double Q2_center = Q2bins[i2];
   //               double t_center  = (tbins[i3]+tbins[i3+1])/2.0;
   //               double P1_center = (P1bins[i4]+P1bins[i4+1])/2.0;
   //               double thetaP1_center = (thetaP1bins[i5]+thetaP1bins[i5+1])/2.0;
   //               //
   //               int    bin_0  = hQ2Vst[0]->FindBin(t_center, Q2_center);
   //               double N_0    = hQ2Vst[0]->GetBinContent(bin_0);
   //               int    bin_1  = hQ2Vsx[0]->FindBin(x_center,1.5);
   //               double N_1    = hQ2Vsx[0]->GetBinContent(bin_1);

   //               //std::cout << "x_center "  << x_center << std::endl;
   //               //std::cout << "Q2_center " << Q2_center << std::endl;
   //               //std::cout << "t_center "  << t_center << std::endl;
   //               sigma0->SetParameters( x_center, Q2_center, -1.0*t_center, P1_center, thetaP1_center );
   //               sigma1->SetParameters( x_center, Q2_center, -1.0*t_center, P1_center, thetaP1_center );
   //               sigma6->SetParameters( x_center, Q2_center, -1.0*t_center, P1_center, thetaP1_center );


   //               TH1  * rel_y1 = sigma0->DrawCopy("goff")->GetHistogram();
   //               TH1  * rel_y2 = sigma1->DrawCopy("goff")->GetHistogram();
   //               hist1->Reset();
   //               hist2->Reset();
   //               int n0 = 1e5;
   //               int n1 = int(n0*N_1/Nref_1);
   //               int n2 = int(n0*N_1/Nref_1);

   //               std::cout << "scale factor " <<  N_1/Nref_1 << std::endl;;

   //               hist1->FillRandom(rel_y1,n1);
   //               hist2->FillRandom(rel_y2,n2);

   //               TH1F * yield = (TH1F *)hist1->Clone("yield");
   //               yield->Add(hist2);
   //               yield->Sumw2(false);
   //               yield->Sumw2(true);

   //               yield->SetLineColor(colors[i5]);
   //               yield->SetLineWidth(2);
   //               yield->SetMarkerStyle(20);
   //               yield->SetMarkerColor(colors[i5]);
   //               yield->SetMarkerSize(0.6);
   //               TGraphErrors * gr = new TGraphErrors(yield);
   //               mg->Add(gr,"ep");
   //               leg->AddEntry(yield,Form("#theta_{s}=%.2f",thetaP1_center),"ep");
   //            }
   //            mg->SetTitle(Form("x=%.2f, Q^{2}=%.2f, t=%.2f, p_{s}=%.2f ",
   //                     (xbins[i1]+xbins[i1+1])/2.0,
   //                     (Q2bins[i2]+Q2bins[i2+1])/2.0,
   //                     (tbins[i3]+tbins[i3+1])/2.0, 
   //                     (P1bins[i4]+P1bins[i4+1])/2.0)
   //                  );
   //            mg->Draw("a");
   //            //mg->GetYaxis()->SetRangeUser(-1.0,1.0);
   //            mg->GetYaxis()->CenterTitle(true);
   //            mg->GetXaxis()->CenterTitle(true);
   //            mg->GetYaxis()->SetTitle("yield");
   //            mg->GetXaxis()->SetTitle("#phi [deg]");
   //            mg->Draw("a");
   //            gPad->SetLogy(true);
   //            mg->Draw("a");
   //            leg->Draw();
   //            c->SaveAs(Form("%s/He4_proton_%d_yields_%d_%d_%d_%d.png",dirname.c_str(),runnumber,,i1,i2,i3,i4));
   //            c->SaveAs(Form("%s/He4_proton_%d_yields_%d_%d_%d_%d.pdf",dirname.c_str(),runnumber,,i1,i2,i3,i4));
   //         }
   //      }
   //   }
   //}

   // ---------------------------------------------
   c   = new TCanvas();
   c->Divide(4,1,0,0);
   c->cd(1);
   gPad->SetRightMargin(0.0001);
   c->cd(2);
   gPad->SetLeftMargin(0.0001);
   gPad->SetRightMargin(0.0001);
   c->cd(3);
   gPad->SetLeftMargin(0.0001);
   gPad->SetRightMargin(0.0001);
   c->cd(4);
   gPad->SetLeftMargin(0.0001);

   for(int i1=0;i1<4;i1++){
      for(int i2=0;i2<4;i2++){
         leg = new TLegend(0.7, 0.7, 0.9, 0.9);
         //c->cd(1+i1+i2*4);
         for(int i3=3;i3>=0;i3--){
            c->cd(i3+1);
            TMultiGraph * mg = new TMultiGraph();
            TH1F * hist1 = hPhiAsymBinned.at(i1).at(i2).at(i3).at(0);
            TH1F * hist2 = hPhiAsymBinned.at(i1).at(i2).at(i3).at(1);
            //if(hist1->GetEntries()<5)continue;
            //if(hist2->GetEntries()<5)continue;
            TH1 * phi_asym = hist1->GetAsymmetry(hist2);
            phi_asym->SetLineColor(1+i3);
            phi_asym->SetLineWidth(2);
            phi_asym->SetMarkerStyle(20);
            phi_asym->SetMarkerColor(1+i3);
            phi_asym->SetMarkerSize(0.6);
            TGraphErrors * gr = new TGraphErrors(phi_asym);
            mg->Add(gr,"ep");
            leg->AddEntry(phi_asym,Form("t=%.2f",(tbins[i3]+tbins[i3+1])/2.0),"ep");

            mg->Draw("a");
            mg->GetYaxis()->SetRangeUser(-1.0,1.0);
            mg->GetYaxis()->SetTitle("A_{LU}");
            mg->GetXaxis()->SetTitle("#phi [deg]");
            mg->GetYaxis()->CenterTitle(true);
            mg->GetXaxis()->CenterTitle(true);
            mg->Draw("a");
         }
         //mg->SetTitle(Form("x=%.2f, Q^{2}=%.2f",(xbins[i1]+xbins[i1+1])/2.0,(Q2bins[i2]+Q2bins[i2+1])/2.0));
         //mg->Draw("a");
         //mg->GetYaxis()->SetRangeUser(-1.0,1.0);
         //mg->GetYaxis()->SetTitle("A_{LU}");
         //mg->GetXaxis()->SetTitle("#phi [deg]");
         //mg->Draw("a");
         //leg->Draw();
         c->SaveAs(Form("%s/He4_proton_asyms_%d_%d_%d.png",dirname.c_str(),runnumber,i1,i2));
         c->SaveAs(Form("%s/He4_proton_asyms_%d_%d_%d.pdf",dirname.c_str(),runnumber,i1,i2));
      }
   }
   //c->SaveAs(Form("%s/He4_proton_%d_asyms.png",dirname.c_str(),runnumber));
   //c->SaveAs(Form("%s/He4_proton_%d_asyms.pdf",dirname.c_str(),runnumber));
   //hPhiAsym[2]->Scale(86400);
   //hPhiAsym[3]->Scale(86400);
   //phi_asym = hPhiAsym[2]->GetAsymmetry(hPhiAsym[3]);
   //for(int ibin = 1; ibin <= phi_asym->GetNbinsX(); ibin++) {
   //   phi_asym->SetBinContent(ibin,0.1);
   //}
   //return ; 
   
   // ---------------------------------------------
   //c   = new TCanvas();
   ////c->Divide(4,4);
   //for(int i1=0;i1<4;i1++){
   //   for(int i2=0;i2<4;i2++){
   //      TMultiGraph * mg = new TMultiGraph();
   //      leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   //      //c->cd(1+i1+i2*4);
   //      for(int i3=0;i3<4;i3++){
   //         TH1F * hist1 = hPhiAsymBinned.at(i1).at(i2).at(i3).at(0);
   //         TH1F * hist2 = hPhiAsymBinned.at(i1).at(i2).at(i3).at(1);
   //         int n1 = hist1->GetEntries()*1000;
   //         int n2 = hist2->GetEntries()*1000;
   //         if(hist1->GetEntries()<1)n1 = 100;
   //         if(hist2->GetEntries()<1)n2 = 100;
   //         TH1F * asym1 = (TH1F*)hist1->Clone("hALU1");
   //         TH1F * asym2 = (TH1F*)hist2->Clone("hALU2");
   //         TH1 * fasym1 = fA_LU->DrawCopy("goff")->GetHistogram();
   //         TH1 * fasym2 = fA_LU2->DrawCopy("goff")->GetHistogram();
   //         asym1->Reset();
   //         asym2->Reset();
   //         asym1->FillRandom(fasym1,n1+n2);
   //         asym2->FillRandom(fasym2,n1+n2);

   //         TH1 * phi_asym = asym1->GetAsymmetry(asym2);
   //         phi_asym->SetLineColor(1+i3);
   //         phi_asym->SetLineWidth(2);
   //         phi_asym->SetMarkerStyle(20);
   //         phi_asym->SetMarkerColor(1+i3);
   //         phi_asym->SetMarkerSize(0.8);
   //         TGraphErrors * gr = new TGraphErrors(phi_asym);
   //         mg->Add(gr,"ep");
   //         leg->AddEntry(phi_asym,Form("t:%.2f-%.2f",tbins[i3],tbins[i3+1]),"ep");
   //      }
   //      mg->SetTitle(Form("x:%.2f-%.2f, Q^{2}:%.2f-%.2f",xbins[i1],xbins[i1+1],Q2bins[i2],Q2bins[i2+1]));
   //      mg->Draw("a");
   //      mg->GetYaxis()->SetTitle("A_{LU}");
   //      mg->GetXaxis()->SetTitle("#phi [deg]");
   //      //mg->GetYaxis()->SetRangeUser(-1.0,1.0);
   //      mg->Draw("a");
   //      leg->Draw();
   //      c->SaveAs(Form("%s/He4_proton_%d_%d_%d_asyms2.png",i1,i2,runnumber));
   //      c->SaveAs(Form("%s/He4_proton_%d_%d_%d_asyms2.pdf",i1,i2,runnumber));
   //   }
   //}
   ////hPhiAsym[2]->Scale(86400);
   ////hPhiAsym[3]->Scale(86400);
   ////phi_asym = hPhiAsym[2]->GetAsymmetry(hPhiAsym[3]);
   ////for(int ibin = 1; ibin <= phi_asym->GetNbinsX(); ibin++) {
   ////   phi_asym->SetBinContent(ibin,0.1);
   ////}
   //c= new TCanvas();
   ////fA_LU->DrawCopy();
   ////fA_LU2->DrawCopy("same");
   //TH1 * fasym1 = fA_LU->DrawCopy("goff")->GetHistogram();
   //TH1 * fasym2 = fA_LU2->DrawCopy("goff")->GetHistogram();
   //TH1F * asym1 = (TH1F*)fasym1->Clone("hALU1");
   //TH1F * asym2 = (TH1F*)fasym2->Clone("hALU2");
   //asym1->Reset();
   //asym2->Reset();
   //asym1->FillRandom(fasym1,2000);
   //asym2->FillRandom(fasym2,2000);
   //asym1->Draw();
   //asym2->Draw("same");
   

   // ---------------------------------------------
   TLatex Tl; Tl.SetTextFont(43); Tl.SetTextSize(20);
   TCanvas * c2   = new TCanvas();
   c   = new TCanvas();
   c->Divide(3,1,0,0);
   c->cd(1);
   gPad->SetRightMargin(0.0001);
   c->cd(2);
   gPad->SetLeftMargin(0.0001);
   gPad->SetRightMargin(0.0001);
   c->cd(3);
   gPad->SetLeftMargin(0.0001);
   for(int i1=0;i1<4;i1++){
      for(int i2=0;i2<4;i2++){

         TMultiGraph * mg_alpha_ratio = new TMultiGraph();
         TMultiGraph * mg_t = new TMultiGraph();
         TLegend * leg_t = new TLegend(0.85, 0.75, 0.999, 0.95); 
         for(int i3=0;i3<4;i3++){

            TMultiGraph * mg_p1 = new TMultiGraph();
            TMultiGraph * mg_p1_alpha_ratio = new TMultiGraph();

            TGraphErrors * gr_t = new TGraphErrors(3);
            gr_t->SetLineColor(colors[i3]);
            gr_t->SetMarkerColor(colors[i3]);
            gr_t->SetLineWidth(2);
            gr_t->SetMarkerStyle(20);
            gr_t->SetMarkerSize(1.1);

            TGraphErrors * gr_ratio = new TGraphErrors(3);
            gr_ratio->SetLineColor(colors[i3]);
            gr_ratio->SetMarkerColor(colors[i3]);
            gr_ratio->SetLineWidth(2);
            gr_ratio->SetMarkerStyle(20);
            gr_ratio->SetMarkerSize(1.1);

            TGraphErrors * gr_trange = new TGraphErrors(3);
            gr_trange->SetLineColor(1);
            gr_trange->SetMarkerColor(1);
            gr_trange->SetLineWidth(2);
            gr_trange->SetMarkerStyle(1);

            // P1
            for(int i4=0;i4<3;i4++){

               TGraphErrors * gr_p1 = new TGraphErrors(3);
               gr_p1->SetLineColor(colors[i4]);
               gr_p1->SetMarkerColor(colors[i4]);
               gr_p1->SetLineWidth(2);
               gr_p1->SetMarkerStyle(20+i4);
               gr_p1->SetMarkerSize(1.2);

               TGraphErrors * gr_p1_ratio = new TGraphErrors(3);
               gr_p1_ratio->SetLineColor(colors[i4]);
               gr_p1_ratio->SetMarkerColor(colors[i4]);
               gr_p1_ratio->SetLineWidth(2);
               gr_p1_ratio->SetMarkerStyle(20+i4);
               gr_p1_ratio->SetMarkerSize(1.2);

               TGraphErrors * gr_p1range = new TGraphErrors(3);
               gr_p1range->SetLineColor(1);
               gr_p1range->SetMarkerColor(1);
               gr_p1range->SetLineWidth(2);
               gr_p1range->SetMarkerStyle(1);

               leg = new TLegend(0.7, 0.7, 0.9, 0.9);

               for(int i5=0;i5<3;i5++){
                  TMultiGraph * mg = new TMultiGraph();
                  c->cd(1+i5);
                  TH1F * hist1 = hPhiAsymBinnedP1ThetaP1.at(i1).at(i2).at(i3).at(i4).at(i5).at(0);
                  TH1F * hist2 = hPhiAsymBinnedP1ThetaP1.at(i1).at(i2).at(i3).at(i4).at(i5).at(1);

                  double x_center  = (xbins[i1]+xbins[i1+1])/2.0;
                  double Q2_center = (Q2bins[i2]+Q2bins[i2+1])/2.0;
                  double t_center  = (tbins[i3]+tbins[i3+1])/2.0;
                  double P1_center = (P1bins[i4]+P1bins[i4+1])/2.0;
                  double thetaP1_center = (thetaP1bins[i5]+thetaP1bins[i5+1])/2.0;

                  int    bin_0  = hQ2Vst[0]->FindBin(t_center, Q2_center);
                  double N_0    = hQ2Vst[0]->GetBinContent(bin_0);
                  int    bin_1  = hQ2Vsx[0]->FindBin(x_center,1.5);
                  double N_1    = hQ2Vsx[0]->GetBinContent(bin_1);

                  sigma0->SetParameters( x_center, Q2_center, -1.0*t_center, P1_center, thetaP1_center );
                  sigma1->SetParameters( x_center, Q2_center, -1.0*t_center, P1_center, thetaP1_center );
                  sigma6->SetParameters( x_center, Q2_center, -1.0*t_center, P1_center, thetaP1_center );

                  TH1  * rel_y1 = sigma0->DrawCopy("goff")->GetHistogram();
                  TH1  * rel_y2 = sigma1->DrawCopy("goff")->GetHistogram();
                  hist1->Reset();
                  hist2->Reset();
                  int n0 = 1e4;
                  int n1 = int(n0*N_1/Nref_1);
                  int n2 = int(n0*N_1/Nref_1);

                  std::cout << "scale factor " <<  N_1/Nref_1 << std::endl;;
                  hist1->FillRandom(rel_y1,n1);
                  hist2->FillRandom(rel_y2,n2);

                  fitAlpha2[i1][i2][i3][i4][i5][0]         = 0.0;
                  fitAlphaErr2[i1][i2][i3][i4][i5][0]      = 0.0;
                  fitAlphaRatio2[i1][i2][i3][i4][i5][0]    = 0.0;
                  fitAlphaRatioErr2[i1][i2][i3][i4][i5][0] = 0.0;

                  //TH1 * phi_asym = asym1->GetAsymmetry(asym2);
                  TH1 * phi_asym = hist1->GetAsymmetry(hist2);
                  phi_asym->SetLineColor(colors[i4]);
                  phi_asym->SetLineWidth(2);
                  phi_asym->SetMarkerStyle(20+i4);
                  phi_asym->SetMarkerColor(colors[i4]);
                  phi_asym->SetMarkerSize(0.8);
                  TGraphErrors * gr = new TGraphErrors(phi_asym);
                  mg->Add(gr,"ep");
                  leg->AddEntry(phi_asym,Form("theta_{s}:%.2f-%.2f",thetaP1bins[i5],thetaP1bins[i5+1]),"ep");

                  phi_asym->Fit(myfunc);

                  double chi2 = myfunc->GetChisquare();
                  double par0 = myfunc->GetParameter(0); //value of 1st parameter
                  double err0 = myfunc->GetParError(0);  //error on first parameter

                  fitAlpha2[i1][i2][i3][i4][i5][0] = par0;
                  fitAlphaErr2[i1][i2][i3][i4][i5][0] = err0;
                  //std::cout << par0 << std::endl;
                  //std::cout << err0 << std::endl;
                  
                  gr_p1->SetPoint(     i5,  i5, i4*0.3);
                  gr_p1->SetPointError(i5,   0, err0);

                  TGraph * gr_fit = new TGraph(myfunc->DrawCopy("goff")->GetHistogram());
                  gr_fit->SetLineWidth(1);
                  gr_fit->SetLineColor(1);
                  mg->Add(gr_fit,"l");
                  
                  // Deuteron - half the statistics
                  //hist1->Reset();
                  //hist2->Reset();
                  //hist1->FillRandom(rel_y1, n1/2.0);
                  //hist2->FillRandom(rel_y2, n2/2.0);
                  //TH1 * phi_asym2 = hist1->GetAsymmetry(hist2);
                  //phi_asym2->Fit(myfunc);
                  //double chi2_D = myfunc->GetChisquare();
                  //double par0_D = myfunc->GetParameter(0); //value of 1st parameter
                  //double err0_D = myfunc->GetParError(0);  //error on first parameter

                  double ratio_err = TMath::Sqrt(err0*err0 + err0*err0);
                  gr_p1_ratio->SetPoint(     i5, i5, 1+0.3*i4);
                  gr_p1_ratio->SetPointError(i5,  0, ratio_err);

                  fitAlphaRatio2[i1][i2][i3][i4][i5][0]    = 1.0;
                  fitAlphaRatioErr2[i1][i2][i3][i4][i5][0] = ratio_err;

                  // draw on divided canvas
                  mg->SetTitle(Form("x:%.2f-%.2f, Q^{2}:%.2f-%.2f, t:%.2f-%.2f, P_{s}:%.2f-%.2f",
                           xbins[i1], xbins[i1+1],
                           Q2bins[i2],Q2bins[i2+1],
                           tbins[i3], tbins[i3+1],
                           P1bins[i4],P1bins[i4+1] ));
                  mg->Draw("a");
                  mg->GetYaxis()->SetTitle("A_{LU}");
                  mg->GetXaxis()->SetTitle("#phi [deg]");
                  mg->GetYaxis()->SetRangeUser(-1.0,1.0);
                  mg->Draw("a");
               } // end loop over theta_s

               // save divided canvas for 
               //leg->Draw();
               c->SaveAs(Form("%s/He4_proton_asyms3_%d_%d_%d_%d_%d.png",dirname.c_str(),runnumber,i1,i2,i3,i4));
               c->SaveAs(Form("%s/He4_proton_asyms3_%d_%d_%d_%d_%d.pdf",dirname.c_str(),runnumber,i1,i2,i3,i4));

               mg_p1->Add(gr_p1,"ep");
               mg_p1_alpha_ratio->Add(gr_p1_ratio,"ep");

            } // loop over p1

            //leg_t->AddEntry(gr_t,Form("t:%.2f-%.2f",tbins[i3],tbins[i3+1]),"ep");
            //mg_t->Add(gr_t,"ep");
            //mg_alpha_ratio->Add(gr_ratio,"ep");

            c2->cd(0);
            mg_p1->SetTitle(Form("x:%.2f-%.2f, Q^{2}:%.2f-%.2f GeV^{2}, t:%.2f-%.2f GeV^{2}", xbins[i1], xbins[i1+1], Q2bins[i2],Q2bins[i2+1],tbins[i3],tbins[i3+1] ));
            mg_p1->Draw("a");
            mg_p1->GetYaxis()->SetTitle("#alpha_{LU}");
            mg_p1->GetXaxis()->SetTitle("#theta_{s}");
            mg_p1->GetYaxis()->CenterTitle(true);
            mg_p1->GetXaxis()->CenterTitle(true);
            mg_p1->GetXaxis()->SetBinLabel(mg_p1->GetXaxis()->FindBin(0.0),"Forward");
            mg_p1->GetXaxis()->SetBinLabel(mg_p1->GetXaxis()->FindBin(1.0),"Perpendicular");
            mg_p1->GetXaxis()->SetBinLabel(mg_p1->GetXaxis()->FindBin(2.0),"Backward");
            mg_p1->GetHistogram()->LabelsDeflate("X");
            mg_p1->GetXaxis()->SetLimits(-0.5,2.5);
            mg_p1->GetYaxis()->SetRangeUser(-0.2,0.8);
            mg_p1->GetXaxis()->LabelsOption("h");
            mg_p1->Draw("a");
            //leg_t->Draw();
            for(int i4=0;i4<3;i4++){
               Tl.DrawLatex(0.5,i4*0.3-0.1,Form("%.2f < P_{s} < %.2f [GeV/c]",P1bins[i4],P1bins[i4+1]) );
            }
            c2->SaveAs(Form("%s/He4_proton_asyms_t_%d_%d_%d_%d.png",dirname.c_str(),runnumber,i1,i2,i3));
            c2->SaveAs(Form("%s/He4_proton_asyms_t_%d_%d_%d_%d.pdf",dirname.c_str(),runnumber,i1,i2,i3));

            mg_p1_alpha_ratio->SetTitle(Form("x:%.2f-%.2f, Q^{2}:%.2f-%.2f GeV^{2}, t:%.2f-%.2f GeV^{2}", xbins[i1], xbins[i1+1], Q2bins[i2],Q2bins[i2+1],tbins[i3],tbins[i3+1] ));
            mg_p1_alpha_ratio->Draw("a");
            mg_p1_alpha_ratio->GetYaxis()->SetTitle("#alpha^{^{4}He}/#alpha^{^{2}H}");
            mg_p1_alpha_ratio->GetXaxis()->SetTitle("#theta_{s}");
            mg_p1_alpha_ratio->GetYaxis()->CenterTitle(true);
            mg_p1_alpha_ratio->GetXaxis()->CenterTitle(true);
            mg_p1_alpha_ratio->GetXaxis()->SetBinLabel(mg_p1->GetXaxis()->FindBin(0.0),"Forward");
            mg_p1_alpha_ratio->GetXaxis()->SetBinLabel(mg_p1->GetXaxis()->FindBin(1.0),"Perpendicular");
            mg_p1_alpha_ratio->GetXaxis()->SetBinLabel(mg_p1->GetXaxis()->FindBin(2.0),"Backward");
            mg_p1_alpha_ratio->GetHistogram()->LabelsDeflate("X");
            mg_p1_alpha_ratio->GetXaxis()->SetLimits(-0.5,2.5);
            mg_p1_alpha_ratio->GetYaxis()->SetRangeUser(0.8,1.8);
            mg_p1_alpha_ratio->GetXaxis()->LabelsOption("h");
            mg_p1_alpha_ratio->Draw("a");
            //leg_t->Draw();
            for(int i4=0;i4<3;i4++){
               Tl.DrawLatex(0.5,i4*0.3+0.9,Form("%.2f < P_{A-1} < %.2f [GeV/c]",P1bins[i4],P1bins[i4+1]) );
            }
            c2->SaveAs(Form("%s/He4_proton_asyms_alpha_ratio_%d_%d_%d_%d.png",dirname.c_str(),runnumber,i1,i2,i3));
            c2->SaveAs(Form("%s/He4_proton_asyms_alpha_ratio_%d_%d_%d_%d.pdf",dirname.c_str(),runnumber,i1,i2,i3));

         } // t loop - i3
      }
   }

   //-------------------------------------------------------
   // Plot of alpha vs x for different Q2 bins
   //-------------------------------------------------------
   c = new TCanvas();
   for(int i2=0;i2<4;i2++){ // Q2
      TMultiGraph * mg_alpha_vs_x = new TMultiGraph();
      TMultiGraph * mg_alphaRatio_vs_x = new TMultiGraph();

      for(int i3=0;i3<4;i3++){ // t
         for(int i1=0;i1<4;i1++){ // x

            for(int i5=0;i5<3;i5++){
               TGraphErrors * gr_alpha_vs_x = new TGraphErrors(3);
               gr_alpha_vs_x->SetMarkerColor(colors[i3]);
               gr_alpha_vs_x->SetMarkerStyle(20+i5);
               gr_alpha_vs_x->SetMarkerSize(0.8);
               gr_alpha_vs_x->SetLineColor(colors[i3]);

               TGraphErrors * gr_alphaRatio_vs_x = new TGraphErrors(3);
               gr_alphaRatio_vs_x->SetMarkerColor(colors[i3]);
               gr_alphaRatio_vs_x->SetMarkerStyle(20+i5);
               gr_alphaRatio_vs_x->SetMarkerSize(0.8);
               gr_alphaRatio_vs_x->SetLineColor(colors[i3]);

               for(int i4=0;i4<3;i4++){

                  double x_center       = (3.0*xbins[i1]+xbins[i1+1])/4.0;
                  double Q2_center      = (3.0*Q2bins[i2]+Q2bins[i2+1])/4.0;
                  double t_center       = (3.0*tbins[i3]+tbins[i3+1])/4.0;
                  double P1_center      = (3.0*P1bins[i4]+P1bins[i4+1])/4.0;
                  double thetaP1_center = (3.0*thetaP1bins[i5]+thetaP1bins[i5+1])/4.0;

                  double val = 0.2 + 0.3*double(i4)-0.02*double(i3);; 
                  double x_offset = 0.01*(i5 +3*i3);
                  if( fitAlpha2[i1][i2][i3][i4][i5][0] == 0 ) {
                     x_offset += 10.0;
                  }
                  if( fitAlphaErr2[i1][i2][i3][i4][i5][0] > 0.25 ) {
                     x_offset += 10.0;
                  }
                  gr_alpha_vs_x->SetPoint(     i4, x_center+x_offset, val);//fitAlpha2[i1][i2][i3][i4][i5][0]);
                  gr_alpha_vs_x->SetPointError(i4, 0, fitAlphaErr2[i1][i2][i3][i4][i5][0]);

                  // alpha ratio
                  val = 0.8 + 0.3*double(i4)-0.02*double(i3);; 
                  x_offset = 0.01*(i5 +3*i3);
                  if( fitAlphaRatio2[i1][i2][i3][i4][i5][0] == 0 ) {
                     x_offset += 10.0;
                  }
                  if( fitAlphaRatioErr2[i1][i2][i3][i4][i5][0] > 0.25 ) {
                     x_offset += 10.0;
                  }
                  gr_alphaRatio_vs_x->SetPoint(     i4, x_center+x_offset, val);//fitAlphaRatio2[i1][i2][i3][i4][i5][0]);
                  gr_alphaRatio_vs_x->SetPointError(i4, 0, fitAlphaRatioErr2[i1][i2][i3][i4][i5][0]);
               }
               mg_alpha_vs_x->Add(gr_alpha_vs_x,"ep");
               mg_alphaRatio_vs_x->Add(gr_alphaRatio_vs_x,"ep");
            }
         }

      }
      mg_alpha_vs_x->Draw("a");
      mg_alpha_vs_x->GetXaxis()->SetLimits(0.0,0.7);
      mg_alpha_vs_x->GetYaxis()->SetRangeUser(0.0,1.0);
      mg_alpha_vs_x->GetXaxis()->SetTitle("x");
      mg_alpha_vs_x->GetXaxis()->CenterTitle(true);
      mg_alpha_vs_x->GetYaxis()->SetTitle("#alpha");
      mg_alpha_vs_x->GetYaxis()->CenterTitle(true);
      mg_alpha_vs_x->Draw("a");
      //for(int i4=0;i4<3;i4++){
      //   Tl.DrawLatex(0.5,i4*0.3-0.1,Form("P_{s} < %.2f [GeV/c]",P1bins[i4],P1bins[i4+1]) );
      //}
      c->SaveAs(Form("%s/He4_proton_alpha_vs_x_%d_%d.png",dirname.c_str(),runnumber,i2));
      c->SaveAs(Form("%s/He4_proton_alpha_vs_x_%d_%d.pdf",dirname.c_str(),runnumber,i2));

      mg_alphaRatio_vs_x->Draw("a");
      mg_alphaRatio_vs_x->GetXaxis()->SetLimits(0.0,0.7);
      mg_alphaRatio_vs_x->GetYaxis()->SetRangeUser(0.3,1.7);
      mg_alphaRatio_vs_x->GetXaxis()->SetTitle("x");
      mg_alphaRatio_vs_x->GetXaxis()->CenterTitle(true);
      mg_alphaRatio_vs_x->GetYaxis()->SetTitle("#alpha^{*}/#alpha");
      mg_alphaRatio_vs_x->GetYaxis()->CenterTitle(true);
      mg_alphaRatio_vs_x->Draw("a");
      //for(int i4=0;i4<3;i4++){
      //   Tl.DrawLatex(0.5,i4*0.3-0.1,Form("P_{s} < %.2f [GeV/c]",P1bins[i4],P1bins[i4+1]) );
      //}
      c->SaveAs(Form("%s/He4_proton_alphaRatio_vs_x_%d_%d.png",dirname.c_str(),runnumber,i2));
      c->SaveAs(Form("%s/He4_proton_alphaRatio_vs_x_%d_%d.pdf",dirname.c_str(),runnumber,i2));
   }


}

