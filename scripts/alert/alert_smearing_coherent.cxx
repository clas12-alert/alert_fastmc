#include "ThrownEvent.h"
#include "CLAS12HitsEvent.h"
#include "ForwardHitMask.h"
#include "ElectronKinematics.h"

#include "../pid_indexing.h"
#include "../CLAS12Resolution.h"
#include "../ALERTResolution.h"

// ----------------------------------------------------------------------
//
// alpha     = 1000020040
// He3       = 1000020030
// triton    = 1000010030
// deuteron  = 1000010020
// proton    = 2212
// neutron   = 2112
//
void alert_smearing_coherent(int runnumber = 50)
{
   using namespace clas12::hits;

   std::string filename     = Form("data/rootfiles/clas12sim%d.root",runnumber);
   std::string filename_out = Form("data/rootfiles/c12sim_smeared_%d.root",runnumber);

   // ----------------------------------------------------------------------
   //
   TFile * f = new TFile(filename.c_str(),"READ");
   f->cd();
   f->ls();
   TTree * t = (TTree*)gROOT->FindObject("clasdigi_hits"); 
   if(!t) {
      std::cout << "error tree not found\n";
      return -1;
   }
   clas12::hits::CLAS12HitsEvent  * event   = 0;
   clas12::sim::ThrownEvent       * thrown  = 0;//new clas12::sim::ThrownEvent();
   kinematics::ElectronKinematics * eKine0  = new kinematics::ElectronKinematics();
   clas12::sim::ThrownEvent       * smeared = new clas12::sim::ThrownEvent();
   kinematics::ElectronKinematics * eKine   = new kinematics::ElectronKinematics();

   t->SetBranchAddress("HitsEvent",   &event);
   t->SetBranchAddress("PrimaryEvent",&thrown);

   // ----------------------------------------------------------------------
   //
   TFile * fout = new TFile(filename_out.c_str(),"RECREATE");
   fout->cd();

   TTree * tout  = new TTree("FastMC","fast mc smeared");
   Int_t alert_accepted = 0;
   tout->Branch("HitsEvent", "clas12::hits::CLAS12HitsEvent", &event);
   tout->Branch("Thrown",    "clas12::sim::ThrownEvent",      &thrown);
   tout->Branch("Smeared",   "clas12::sim::ThrownEvent",      &smeared);
   tout->Branch("eKine0",   "kinematics::ElectronKinematics", &eKine0);
   tout->Branch("eKine",   "kinematics::ElectronKinematics", &eKine);
   tout->Branch("alert_accepted", &alert_accepted, "alert_accepted/I");

   TRandom3 rand;
   rand.SetSeed();

   eKine->fM1  = 3.7284;
   eKine0->fM1 = 3.7284;
   eKine->Get_k1()  = { 0.0, 0.0, 11.0, 11.0 };
   eKine0->Get_k1() = { 0.0, 0.0, 11.0, 11.0 };
   eKine->Get_p1()  = { 0.0, 0.0, 0.0, 3.7284 };
   eKine0->Get_p1() = { 0.0, 0.0, 0.0, 3.7284 };

   // ----------------------------------------------------------------------
   //
   int nevents = t->GetEntries();
   for(int ievent = 0; ievent < nevents; ievent++){

      smeared->Clear();
      alert_accepted = 0;
      //mask->Clear();

      t->GetEntry(ievent);

      // ---------------------------------------------------------
      // Event info
      thrown->fRunNumber           = event->fRunNumber;
      event->fHitMask.fRunNumber   = event->fRunNumber;
      event->fHitMask.fEventNumber = event->fEventNumber;

      // ---------------------------------------------------------
      // Count the hits
      int n_dc_hits = event->fDCEvent.fNParticleHits;
      int n_ec_hits = event->fECEvent.fNRegionHits;
      int n_rc_hits = event->fRCEvent.fNParticleHits;
      int n_rh_hits = event->fRHEvent.fNParticleHits;
      int n_thrown  = thrown->fNParticles;


      // ---------------------------------------------------------
      // DC
      // The DCEvent ParticleHit array contains a hit for every
      // particle to cross (enter) a DC unit cell. For each hit
      // the DC wire is recorded (see the fDCWire members below).
      // The geant4 track number is also recorded (and starts at 1)
      // this is used to match particle trajectories with the hits.
      // Here we simply count the number of DC wire hits for each 
      // track. Ideally there would be 36 hits = 6(layer)x6(sl)
      // for a track. This is an easy cut for selecting good tracks
      // in clas12.
      for(int ihit = 0; ihit < n_dc_hits; ihit++){

         auto phit         = event->fDCEvent.GetParticleHit(ihit);
         int  track_id     = phit->fTrackID - 1;

         //int  sector       = phit->fDCWire.fSector;
         //int  region       = phit->fDCWire.fRegion;
         //int  sl           = phit->fDCWire.fSuperLayer;
         //int  layer        = phit->fDCWire.fLayer;
         //int  wire         = phit->fDCWire.fLayer;
         
         auto trk_hit_mask = event->TrackHitMask(track_id);
         if( !trk_hit_mask ) {
            std::cout << "no mask\n";
         } else {
            trk_hit_mask->fDC++;
            event->fHitMask.fDC++;
         }

      }

      //std::cout << " EC hits \n";
      // ---------------------------------------------------------
      // EC  
      for(int ihit = 0; ihit < n_ec_hits; ihit++){
         auto phit      = event->fECEvent.GetRegionHit(ihit);
         int  track_id  = phit->fTrackID - 1;

         auto trk_hit_mask = event->TrackHitMask(track_id);
         if( !trk_hit_mask ) {
            std::cout << "no mask\n";
         } else {
            trk_hit_mask->fEC++;
            event->fHitMask.fEC++;
            if(phit->fPDGCode == 11) {
               trk_hit_mask->fElectron++;
               event->fHitMask.fElectron++;
            }
         }
      }

      //if( event->fHitMask.fEC ) {
      //      event->fHitMask.fForwardStatus = 1;
      //}

      // ---------------------------------------------------------
      // RC  
      // Recoil chamber is the same as DC with the caveat that
      // each unit cell is composed of 4 sub-cell volumes each
      // generating a particle hit. These can be counted and 
      // used as a cut (>8) as well. 
      //
      for(int ihit = 0; ihit < n_rc_hits; ihit++){

         auto * phit     = event->fRCEvent.GetParticleHit(ihit);
         int    track_id = phit->fTrackID - 1;

         auto trk_hit_mask = event->TrackHitMask(track_id);
         if( !trk_hit_mask ) {
            std::cout << "no mask\n";
         } else {
            trk_hit_mask->fRC++;
            event->fHitMask.fRC++;
         }
      }

      // ---------------------------------------------------------
      // RH  
      // Recoil Hodoscope also records particle hits when a track
      // enters the scintillator volume. For alpha particles
      // we expect only one hit in the first layer, however,
      // the fastmc has no material so it also generates hits
      // hits beyond the first layer. This maybe possibly fixed
      // in the future but is not currently a problem.
      //
      for(int ihit = 0; ihit < n_rh_hits; ihit++){
         auto * phit     = event->fRHEvent.GetParticleHit(ihit);
         int    track_id = phit->fTrackID - 1;

         auto trk_hit_mask = event->TrackHitMask(track_id);
         if( !trk_hit_mask ) {
            std::cout << "no mask\n";
         } else {
            trk_hit_mask->fRH++;
            event->fHitMask.fRH++;
         }
      }

      //if( event->fHitMask.fRC ) {
      //      event->fHitMask.fCentralStatus = 1;
      //}

      //event->fHitMask.fStatus  = (event->fHitMask.fForwardStatus && event->fHitMask.fCentralStatus);

      // ---------------------------------------------------------
      // Smeared event
      //std::cout << " thrown \n";
      for(int ithrown = 0; ithrown<n_thrown; ithrown++){

         auto p0 = thrown->GetParticle(ithrown);
         smeared->AddParticle(p0);

         //std::cout << p0->GetPdgCode() << std::endl;
         auto hm = event->TrackHitMask(ithrown);
         //hm->Print();

         auto p1 = smeared->GetParticle(ithrown);

         TLorentzVector  P_thrown;
         p1->Momentum( P_thrown );

         clas12::SmearThrownParticle(*p0, *p1);
         //std::cout << (p0->P() - p1->P()) << std::endl;;
         //
         TLorentzVector  P_smeared;
         p1->Momentum( P_smeared );

         if( p1->GetPdgCode() ==   11 ) {
            // electron
            eKine->Get_k2() = P_smeared;
            eKine0->Get_k2() = P_thrown;

         } else if( (p1->GetPdgCode() == 2112) ) {
            // neutron
            //eKine->Get_p2() = P_smeared;
            //eKine0->Get_p2() = P_thrown;

         } else if( (p1->GetPdgCode() == 2212) ) {
            // proton
            eKine->Get_p2() = P_smeared;
            eKine0->Get_p2() = P_thrown;

         } else if( (p1->GetPdgCode() == 22) ) {
            // photon
            //P_smeared.Print();
            eKine->Get_p0() = P_smeared;
            eKine0->Get_p0() = P_thrown;

         } else if( (p1->GetPdgCode() == 1000020040) ) {
            // He4
            if( alert::AcceptThrownParticle(*p0) == false ){
               // He4 would not make it to the detector
               auto trk_hit_mask = event->TrackHitMask(ithrown);
               trk_hit_mask->fRH   = 0;
               event->fHitMask.fRH = 0;
               trk_hit_mask->fRC   = 0;
               event->fHitMask.fRC = 0;
            }
            eKine->Get_p2() = P_smeared;
            eKine0->Get_p2() = P_thrown;


         }

      }

      tout->Fill();
   }

   fout->Write();

}

