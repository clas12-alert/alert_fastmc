#include "ThrownEvent.h"
#include "CLAS12HitsEvent.h"
#include "ForwardHitMask.h"
#include "ElectronKinematics.h"
#include "CLHEP/Units/SystemOfUnits.h"

#include "../pid_indexing.h"
#include "../ALERTResolution.h"
#include "../CLAS12Resolution.h"

// ----------------------------------------------------------------------
//
// alpha     = 1000020040
// He3       = 1000020030
// triton    = 1000010030
// deuteron  = 1000010020
// proton    = 2212
//
void coherent_dvcs_He4(int runnumber = 40110)
{
   using namespace clas12::hits;
   using namespace CLHEP;

   std::string filename     = Form("data/rootfiles/c12sim_smeared_%d.root",runnumber);
   std::string filename_out = Form("data/rootfiles/c12_smeared_%d.root",runnumber);

   // ----------------------------------------------------------------------
   //
   clas12::hits::CLAS12HitsEvent * event   = 0;
   clas12::sim::ThrownEvent      * thrown  = 0;//new clas12::sim::ThrownEvent();
   clas12::sim::ThrownEvent      * smeared = new clas12::sim::ThrownEvent();
   kinematics::ElectronKinematics * eKine0  = new kinematics::ElectronKinematics();
   kinematics::ElectronKinematics * eKine  = new kinematics::ElectronKinematics();

   //t->SetBranchAddress("HitsEvent",   &event);
   //t->SetBranchAddress("PrimaryEvent",&thrown);

   // ----------------------------------------------------------------------
   //
   TFile * f = new TFile(filename.c_str(),"READ");
   f->cd();

   TTree * t = (TTree*)gROOT->FindObject("FastMC"); 
   Int_t alert_accepted = 0;
   t->SetBranchAddress("HitsEvent", &event);
   t->SetBranchAddress("Thrown",    &thrown);
   t->SetBranchAddress("Smeared",   &smeared);
   t->SetBranchAddress("eKine0",     &eKine0);
   t->SetBranchAddress("eKine",     &eKine);

   TRandom3 rand;
   rand.SetSeed();

   // ----------------------------------------------------------------------
   //
   TFile * fout = new TFile(filename_out.c_str(),"UPDATE");
   

   std::vector<TH1F*> hDeltaE = {
      new TH1F("hDeltaE0", "E", 100, -1, 1),
      new TH1F("hDeltaE1", "E", 100, -1, 1),
      new TH1F("hDeltaE2", "E", 100, -1, 1),
      new TH1F("hDeltaE3", "E", 100, -1, 1),
      new TH1F("hDeltaE4", "E", 100, -1, 1)
   };
   std::vector<TH1F*> hDeltaTheta = {
      new TH1F("hDeltaTheta0", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta1", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta2", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta3", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta4", "#Delta#theta", 100, -10, 10)
   };

   std::vector<TH1F*> hPhi = {
      new TH1F("hPhi0", "#phi", 100, 0, 360),
      new TH1F("hPhi1", "#phi", 100, 0, 360),
      new TH1F("hPhi2", "#phi", 100, 0, 360),
      new TH1F("hPhi3", "#phi", 100, 0, 360),
      new TH1F("hPhi4", "#phi", 100, 0, 360)
   };

   std::vector<TH1F*> hPhiAsym = {
      new TH1F("hPhiAsym0", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym1", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym2", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym3", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym4", "#phi", 20, -180, 180),
      new TH1F("hPhiAsym5", "#phi", 20, -180, 180)
   };

   std::vector<TH2F*> hThetaPhi = {
      new TH2F("hThetaPhi0", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi1", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi2", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi3", "#theta vs #phi", 180, 0, 360, 90, 0, 180),
      new TH2F("hThetaPhi4", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi5", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi6", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi7", "#theta vs #phi", 180, 0, 360, 90, 0, 180),
      new TH2F("hThetaPhi8", "#theta vs #phi", 180, 0, 360, 90, 0, 180)
   };

   std::vector<TH2F*> hThetaP = {
      new TH2F("hThetaP0", "#theta vs P", 100, 0, 11, 70, 0, 70),
      new TH2F("hThetaP1", "#theta vs P", 100, 0, 11, 70, 0, 70),
      new TH2F("hThetaP2", "#theta vs P", 100, 0, 11, 70, 0, 70),
      new TH2F("hThetaP3", "#theta vs P", 100, 0, 1,  90, 0, 180),
      new TH2F("hThetaP4", "#theta vs P", 100, 0, 11, 90, 0, 180)
   };

   std::vector<TH1F*> hE_e = {
      new TH1F("hE0_e", "E", 100, 0, 11),
      new TH1F("hE1_e", "E", 100, 0, 11),
      new TH1F("hE2_e", "E", 100, 0, 11),
      new TH1F("hE3_e", "E", 100, 0, 11)
   };
   std::vector<TH1F*> hE_He4 = {
      new TH1F("hE_He40", "E", 100, 0, 11),
      new TH1F("hE_He41", "E", 100, 0, 11),
      new TH1F("hE_He42", "E", 100, 0, 11),
      new TH1F("hE_He43", "E", 100, 0, 11)
   };
   std::vector<TH1F*> hE_p = {
      new TH1F("hE0_p", "E proton", 100, 0, 11.0),
      new TH1F("hE1_p", "E proton", 100, 0, 11.0),
      new TH1F("hE2_p", "E proton", 100, 0, 11.0),
      new TH1F("hE3_p", "E proton", 100, 0, 11.0)
   };
   std::vector<TH1F*> hE_g = {
      new TH1F("hE0_g", "E", 100, 0, 11),
      new TH1F("hE1_g", "E", 100, 0, 11),
      new TH1F("hE2_g", "E", 100, 0, 11),
      new TH1F("hE3_g", "E", 100, 0, 11)
   };

   std::vector<TH1F*> hTheta_e = {
      new TH1F("hTheta0_e", "#theta", 90, 0, 180),
      new TH1F("hTheta1_e", "#theta", 90, 0, 180),
      new TH1F("hTheta2_e", "#theta", 90, 0, 180),
      new TH1F("hTheta3_e", "#theta", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_He4 = {
      new TH1F("hTheta0_He4", "#theta", 90, 0, 180),
      new TH1F("hTheta1_He4", "#theta", 90, 0, 180),
      new TH1F("hTheta2_He4", "#theta", 90, 0, 180),
      new TH1F("hTheta3_He4", "#theta", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_p = {
      new TH1F("hTheta0_p", "Theta proton", 90, 0, 180),
      new TH1F("hTheta1_p", "Theta proton", 90, 0, 180),
      new TH1F("hTheta2_p", "Theta proton", 90, 0, 180),
      new TH1F("hTheta3_p", "Theta proton", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_g = {
      new TH1F("hTheta0_g", "Theta", 90, 0, 180),
      new TH1F("hTheta1_g", "Theta", 90, 0, 180),
      new TH1F("hTheta2_g", "Theta", 90, 0, 180),
      new TH1F("hTheta3_g", "Theta", 90, 0, 180)
   };

   std::vector<TH1F*> hE0 = {
      new TH1F("hE00", "E_{#gamma}", 100, 0.0, 4),
      new TH1F("hE01", "E_{#gamma}", 100, 0.0, 4),
      new TH1F("hE02", "E_{#gamma}", 100, 0.0, 4),
      new TH1F("hE03", "E_{#gamma}", 100, 0.0, 4)
   };

   std::vector<TH1F*> hQ2 = {
      new TH1F("hQ20", "Q^{2}", 100, 0, 4),
      new TH1F("hQ21", "Q^{2}", 100, 0, 4),
      new TH1F("hQ22", "Q^{2}", 100, -0.2, 0.2),
      new TH1F("hQ23", "Q^{2}", 100, 0, 4),
      new TH1F("hQ24", "Q^{2}", 100, 0, 4),
      new TH1F("hQ25", "Q^{2}", 100, -0.2, 0.2)
   };
   std::vector<TH1F*> hx = {
      new TH1F("hx0", "x", 100, 0, 1),
      new TH1F("hx1", "x", 100, 0, 1),
      new TH1F("hx2", "x", 100, -0.5, 0.5),
      new TH1F("hx3", "x", 100, 0, 1),
      new TH1F("hx4", "x", 100, 0, 1),
      new TH1F("hx5", "x", 100, -0.5, 0.5),
      new TH1F("hx6", "x", 100, -0.5, 0.5)
   };
   std::vector<TH1F*> hW = {
      new TH1F("hW0", "W", 100, 0.5, 4),
      new TH1F("hW1", "W", 100, 0.5, 4),
      new TH1F("hW2", "W", 100,-0.2, 0.2),
      new TH1F("hW3", "W", 100, 0.5, 4),
      new TH1F("hW4", "W", 100, 0.5, 4),
      new TH1F("hW5", "W", 100,-0.2, 0.2),
      new TH1F("hW6", "W", 100,-0.2, 0.2)
   };
   std::vector<TH1F*> ht = {
      new TH1F("ht0", "t", 100, -1.0, 0.0),
      new TH1F("ht1", "t", 100, -1.0, 0.0),
      new TH1F("ht2", "t", 100, -1.0, 1.0),
      new TH1F("ht3", "t", 100, -1.0, 0.0),
      new TH1F("ht4", "t", 100, -1.0, 0.0),
      new TH1F("ht5", "t", 100, -1.0, 1.0),
      new TH1F("ht6", "t", 100, -1.0, 1.0)
   };
   std::vector<TH2F*> hDeltatVst = {
      new TH2F("hDeltatVst0", "#deltat Vs t", 100, -5, 5, 100, -5, 5),
      new TH2F("hDeltatVst1", "#deltat Vs t", 100, -5, 5, 100, -5, 5),
      new TH2F("hDeltatVst2", "exact t Vs approximate t", 100, -5, 0, 100, -5, 5),
      new TH2F("hDeltatVst3", "#deltat Vs t", 100, -5, 5, 100, -5, 5),
      new TH2F("hDeltatVst4", "#deltat Vs t", 100, -5, 5, 100, -5, 5)
   };

   std::vector<TH2F*> hQ2Vsx = {
      new TH2F("hQ2Vsx0", "Q^{2} vs x", 100, 0, 1, 100, 0, 5),
      new TH2F("hQ2Vsx1", "Q^{2} vs x", 100, 0, 1, 100, 0, 5),
      new TH2F("hQ2Vsx2", "Q^{2} vs x", 100, 0, 1, 100, 0, 5),
      new TH2F("hQ2Vsx3", "Q^{2} vs x", 100, 0, 1, 100, 0, 5),
      new TH2F("hQ2Vsx4", "Q^{2} vs x", 100, 0, 1, 100, 0, 5)
   };
   // ----------------------------------------------------------------------
   // Event loop 
   int nevents = t->GetEntries();
   std::cout << nevents << " Events." << std::endl;
   for(int ievent = 0; ievent < nevents; ievent++){

      t->GetEntry(ievent);

      int n_thrown  = thrown->fNParticles;

      int helicity = int(thrown->fBeamPol);

      int status = 0;

      bool good_proton  = false;
      bool good_gamma   = false;
      bool good_e       = false;
      bool good_triton  = false;
      bool good_alpha  = false;

      TLorentzVector  p_tot = {0,0,0.0,0.0};
      // ------------------------------------------------------------------
      // 
      for(int ithrown = 0; ithrown<n_thrown; ithrown++){

         auto p0 = thrown->GetParticle( ithrown);
         auto p1 = smeared->GetParticle(ithrown);
         //std::cout << p0->GetPdgCode() << std::endl;;

         auto hm = event->TrackHitMask(ithrown);
         if(!hm) continue;
         //if( hm->fDC < 18 ) continue;

         bool good_dc  = false;
         bool good_rc  = false;
         bool good_rh  = false;
         bool good_all = false;

         // hm->fDC
         if( hm->fDC >30) {
            good_dc = true;
         }
         if( hm->fRC >7) {
            good_rc = true;
         }
         if( hm->fRH >0) {
            good_rh = true;
         }
         if( good_dc /*&& good_rh && good_rc*/ ){
            good_all = true;
         }


         if( p0->GetPdgCode() == 11 ) {

            hE_e       [0]->Fill( p0->Energy() );
            hTheta_e   [0]->Fill( p0->Theta()/degree );

            if(good_dc){

               status++;
               good_e = true;

               hE_e       [1]->Fill(p1->Energy() );
               hTheta_e   [1]->Fill(p1->Theta()/degree );

               hThetaP    [0]->Fill(p1->P(), p1->Theta()/degree);
               hThetaPhi  [0]->Fill(p1->Phi()/degree, p0->Theta()/degree);
               hPhi       [0]->Fill(p1->Phi()/degree);

               hDeltaE    [0]->Fill(p0->P()- p1->P());
               hDeltaTheta[0]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }

         } else if( p0->GetPdgCode() == 2212 ) {

            hE_p    [0]->Fill( p0->Energy() );
            hTheta_p[0]->Fill( p0->Theta()/degree );

            if(good_dc){

               status++;
               good_proton = true;

               hE_p       [1]->Fill( p1->Energy() );
               hTheta_p   [1]->Fill( p1->Theta()/degree );

               hThetaP    [1]->Fill(p1->P(), p1->Theta()/degree);
               hThetaPhi  [1]->Fill(p1->Phi()/degree,p1->Theta()/degree);
               hPhi       [1]->Fill(p1->Phi()/degree);

               hDeltaE    [1]->Fill(p0->P() - p1->P());
               hDeltaTheta[1]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }
         } else if( p0->GetPdgCode() == 22 ) {

            hE_g[0]->Fill( p0->Energy() );
            hTheta_g[0]->Fill( p0->Theta()/degree );

            if(good_dc){

               status++;
               good_gamma = true;

               hE_g    [1]->Fill( p1->Energy() );
               hTheta_g[1]->Fill( p1->Theta()/degree );

               hThetaP  [2]->Fill(p1->P(), p1->Theta()/degree);
               hThetaPhi[2]->Fill(p1->Phi()/degree, p1->Theta()/degree);
               hPhi     [2]->Fill(p1->Phi()/degree);

               hDeltaE    [2]->Fill(p0->P()- p1->P());
               hDeltaTheta[2]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }

         } else if( (p0->GetPdgCode() == 1000020040) ) {

            hE_He4    [0]->Fill( p0->P() );
            hTheta_He4[0]->Fill( p0->Theta()/degree );

            if(good_rh && (p1->P() < 1.5) ){

               status++;
               good_alpha = true;

               hE_He4    [1]->Fill( p1->P() );
               hTheta_He4[1]->Fill( p1->Theta()/degree );

               hThetaP  [3]->Fill(p1->P(), p1->Theta()/degree);
               hThetaPhi[3]->Fill(p1->Phi()/degree, p1->Theta()/degree);
               hPhi     [3]->Fill(p1->Phi()/degree);

               hDeltaE    [3]->Fill(p0->P()- p1->P());
               hDeltaTheta[3]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }
         }

      }

      bool good_coherent_event = (good_e && good_gamma && good_alpha );

      // -------------------------------------------------
      //
      if( (eKine->p2().Vect().Mag()<1.0) &&(status > 1) && (good_coherent_event) ) {

         //auto P_in  = eKine0->k1().Vect();
         //auto P_out = eKine0->k2().Vect();
         //P_out     += eKine0->p0().Vect();
         //P_out     += eKine0->p2().Vect();
         //auto P_tot = P_in - P_out;
         //P_tot.Print();

         double t_thrown  = eKine0->t_true();
         double W_thrown  = eKine0->W();
         double Q2_thrown = eKine0->Q2();
         double x_thrown  = eKine0->x();

         double t_recon  = eKine->t_true();
         double W_recon  = eKine->W();
         double Q2_recon = eKine->Q2();
         double x_recon  = eKine->x();

         double delta_x  = (x_thrown  - x_recon )/x_thrown;
         double delta_t  = (t_thrown  - t_recon )/t_thrown;
         double delta_W  = (W_thrown  - W_recon )/W_thrown;
         double delta_Q2 = (Q2_thrown - Q2_recon)/Q2_thrown;

         hQ2Vsx[0]->Fill(x_thrown, Q2_thrown);

         hx    [0]->Fill( x_thrown );
         hQ2   [0]->Fill( Q2_thrown  );
         hW    [0]->Fill( W_thrown  );
         ht    [0]->Fill( t_thrown );

         hx    [1]->Fill( x_recon );
         hQ2   [1]->Fill( Q2_recon   );
         hW    [1]->Fill( W_recon    );
         ht    [1]->Fill( t_recon );

         hx [2]->Fill( delta_x );
         hQ2[2]->Fill( delta_Q2 );
         hW [2]->Fill( delta_W );
         ht [2]->Fill( delta_t );
         
         //hDeltatVst[0]->Fill(t_recon , t_diff_recon);
         //hDeltatVst[1]->Fill(t_approx_recon, t_diff_recon);
         //hDeltatVst[2]->Fill(t_recon, t_approx_recon);

         if(helicity == 0) {
            hPhiAsym[0]->Fill(eKine0->phi_dvcs()/degree);
            hPhiAsym[1]->Fill(eKine0->phi_dvcs()/degree);
         }else if(helicity == 1) {
            hPhiAsym[0]->Fill(eKine0->phi_dvcs()/degree);
         }else if(helicity == -1) {
            hPhiAsym[1]->Fill(eKine0->phi_dvcs()/degree);
         }

         // -------------------------------------------------
         // High p events
         if( Q2_recon > 1.0 ) {

            hQ2Vsx[1]->Fill(x_thrown, Q2_thrown);

            hThetaPhi[4]->Fill(eKine->k2().Theta()/degree, eKine->k2().Phi()/degree);
            hThetaPhi[6]->Fill(eKine->p0().Theta()/degree, eKine->p0().Phi()/degree);
            hThetaPhi[7]->Fill(eKine->p2().Theta()/degree, eKine->p2().Phi()/degree);

            hE_He4[2]->Fill( eKine0->p2().Vect().Mag() );
            hE_g  [2]->Fill( eKine0->p0().Vect().Mag() );
            hE_e  [2]->Fill( eKine0->k2().Vect().Mag() );

            hTheta_He4[2]->Fill( eKine0->p2().Vect().Theta()/degree);
            hTheta_g  [2]->Fill( eKine0->p0().Vect().Theta()/degree);
            hTheta_e  [2]->Fill( eKine0->k2().Vect().Theta()/degree);

            ht[3]->Fill(t_thrown);
            ht[4]->Fill(t_recon);
            ht[5]->Fill(delta_t);

            hx[3]->Fill(x_thrown);
            hx[4]->Fill(x_recon);
            hx[5]->Fill(delta_x);

            hW[3]->Fill(W_thrown);
            hW[4]->Fill(W_recon);
            hW[5]->Fill(delta_W);

            hQ2[3]->Fill(Q2_thrown);
            hQ2[4]->Fill(Q2_recon);
            hQ2[5]->Fill(delta_Q2);

            if(helicity == 0) {
               hPhiAsym[2]->Fill(eKine0->phi_dvcs()/degree);
               hPhiAsym[3]->Fill(eKine0->phi_dvcs()/degree);
            }else if(helicity == 1) {
               hPhiAsym[2]->Fill(eKine0->phi_dvcs()/degree);
            }else if(helicity == -1) {
               hPhiAsym[3]->Fill(eKine0->phi_dvcs()/degree);
            }
         }


      }
   }
   fout->Write();

   // ----------------------------------------------------------------------
   // 
   TCanvas * c   = 0;
   TLegend * leg = 0;
   THStack * hs  = 0;
   std::vector<int> colors = {1,2,4,6,7,3,8,9,30,34,37,41,46};

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.3, 0.7, 0.6, 0.9);
   hs  = new THStack("energies","P");
   gPad->SetLogy(true);
   for(int i = 0; i<2; i++) {
      hE_e[i]->SetLineWidth(2);
      hE_p[i]->SetLineWidth(2);
      hE_g[i]->SetLineWidth(2);
      hE_He4[i]->SetLineWidth(2);

      hE_e[i]->SetLineColor(colors[i]);
      hE_p[i]->SetLineColor(colors[i+2]);
      hE_g[i]->SetLineColor(colors[i+4]);
      hE_He4[i]->SetLineColor(colors[i+6]);

      hs->Add(hE_e[i]);
      hs->Add(hE_p[i]);
      hs->Add(hE_g[i]);
      hs->Add(hE_He4[i]);
   }
   leg->SetNColumns(2);
   leg->AddEntry(hE_e[0], "e- thrown",     "l");
   leg->AddEntry(hE_e[1], "e- smeared",     "l");
   leg->AddEntry(hE_p[0], "p thrown",      "l");
   leg->AddEntry(hE_p[1], "p  smeared",      "l");
   leg->AddEntry(hE_g[0], "#gamma thrown", "l");
   leg->AddEntry(hE_g[1], "#gamma smeared", "l");
   leg->AddEntry(hE_He4[0], "recoil thrown", "l");
   leg->AddEntry(hE_He4[1], "recoil smeared", "l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("P [GeV/c]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_0.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_0.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.65, 0.65, 0.85, 0.9);
   hs  = new THStack("theta_hists","#theta");
   gPad->SetLogy(true);
   for(int i = 0; i<2; i++) {
      hTheta_e[i]->SetLineWidth(2);
      hTheta_p[i]->SetLineWidth(2);
      hTheta_g[i]->SetLineWidth(2);
      hTheta_He4[i]->SetLineWidth(2);

      hTheta_e[i]->SetLineColor(colors[i]);
      hTheta_p[i]->SetLineColor(colors[i+2]);
      hTheta_g[i]->SetLineColor(colors[i+4]);
      hTheta_He4[i]->SetLineColor(colors[i+6]);

      hs->Add(hTheta_e[i]);
      hs->Add(hTheta_p[i]);
      hs->Add(hTheta_g[i]);
      hs->Add(hTheta_He4[i]);
   }
   leg->SetNColumns(2);
   leg->AddEntry(hTheta_e[0], "e- thrown",     "l");
   leg->AddEntry(hTheta_e[1], "e- smeared",     "l");
   leg->AddEntry(hTheta_p[0], "p thrown",      "l");
   leg->AddEntry(hTheta_p[1], "p  smeared",      "l");
   leg->AddEntry(hTheta_g[0], "#gamma thrown", "l");
   leg->AddEntry(hTheta_g[1], "#gamma smeared", "l");
   leg->AddEntry(hTheta_He4[0], "recoil thrown", "l");
   leg->AddEntry(hTheta_He4[1], "recoil smeared", "l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta [deg]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_1.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_1.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("Whists","W");

   for(int i = 0; i<2; i++) {
      hW[i]->SetLineColor(colors[i]);
      hs->Add(hW[i]);
   }
   for(int i = 3; i<5; i++) {
      hW[i]->SetLineColor(colors[i]);
      hs->Add(hW[i]);
   }
   leg->AddEntry(hW[0],"thrown");
   leg->AddEntry(hW[1],"recon");
   leg->AddEntry(hW[3],"with cut thrown");
   leg->AddEntry(hW[4],"with cut recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("W [GeV]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_2.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_2.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("xhists","x");

   for(int i = 0; i<2; i++) {
      hx[i]->SetLineColor(colors[i]);
      hs->Add(hx[i]);
   }
   for(int i = 3; i<5; i++) {
      hx[i]->SetLineColor(colors[i]);
      hs->Add(hx[i]);
   }
   leg->AddEntry(hx[0],"thrown");
   leg->AddEntry(hx[1],"recon");
   leg->AddEntry(hx[3],"with cut thrown");
   leg->AddEntry(hx[4],"with cut recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("x_{B}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_3.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_3.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("Q2hists","Q^{2}");

   for(int i = 0; i<2; i++) {
      hQ2[i]->SetLineColor(colors[i]);
      hs->Add(hQ2[i]);
   }
   for(int i = 3; i<5; i++) {
      hQ2[i]->SetLineColor(colors[i]);
      hs->Add(hQ2[i]);
   }
   leg->AddEntry(hQ2[0],"thrown");
   leg->AddEntry(hQ2[1],"recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_4.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_4.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.2, 0.7, 0.4, 0.9);
   hs  = new THStack("thists","t");
   for(int i = 0; i<2; i++) {
      ht[i]->SetLineColor(colors[i]);
      hs->Add(ht[i]);
   }
   for(int i = 3; i<5; i++) {
      ht[i]->SetLineColor(colors[i]);
      hs->Add(ht[i]);
   }
   leg->AddEntry(ht[0],"thrown");
   leg->AddEntry(ht[1],"recon");
   leg->AddEntry(ht[3],"with cut thrown");
   leg->AddEntry(ht[4],"with cut recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("t [GeV^{2}]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_5.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_5.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaEhists","#Delta P");

   for(int i = 0; i<4; i++) {
      hDeltaE[i]->SetLineColor(colors[i]);
      hs->Add(hDeltaE[i]);
   }
   leg->AddEntry(hDeltaE[0],"electrons");
   leg->AddEntry(hDeltaE[2],"photons");
   leg->AddEntry(hDeltaE[3],"alphas");
   leg->Draw();
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("P_{thrown} - P_{recon} [GeV]");
   hs->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_6.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_6.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   hs  = new THStack("deltaTHetahists","#Delta #theta");
   hDeltaTheta[0]->SetLineColor(colors[0]);
   hDeltaTheta[2]->SetLineColor(colors[2]);
   hDeltaTheta[3]->SetLineColor(colors[3]);
   hs->Add(hDeltaTheta[0]);
   hs->Add(hDeltaTheta[2]);
   hs->Add(hDeltaTheta[3]);
   hs->Draw("nostack");
   hs->Draw("colz");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta_{thrown} - #theta_{recon} [deg]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_7.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_7.pdf",runnumber));

   // ---------------------------------------------
   // Theta Phi
   c   = new TCanvas();
   hThetaPhi[0]->Draw("colz");
   hThetaPhi[0]->GetXaxis()->CenterTitle(true);
   hThetaPhi[0]->GetYaxis()->CenterTitle(true);
   hThetaPhi[0]->GetXaxis()->SetTitle("#phi_{e} [deg]");
   hThetaPhi[0]->GetYaxis()->SetTitle("#theta_{e} [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_8.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_8.pdf",runnumber));

   //// ---------------------------------------------
   //c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   //hThetaPhi[1]->Draw("colz");
   //hThetaPhi[1]->GetXaxis()->CenterTitle(true);
   //hThetaPhi[1]->GetYaxis()->CenterTitle(true);
   //hThetaPhi[1]->GetXaxis()->SetTitle("#phi_{p} [deg]");
   //hThetaPhi[1]->GetYaxis()->SetTitle("#theta_{p} [deg]");
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_81.png",runnumber));
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_81.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[2]->Draw("colz");
   hThetaPhi[2]->GetXaxis()->CenterTitle(true);
   hThetaPhi[2]->GetYaxis()->CenterTitle(true);
   hThetaPhi[2]->GetXaxis()->SetTitle("#phi_{#gamma} [deg]");
   hThetaPhi[2]->GetYaxis()->SetTitle("#theta_{#gamma} [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_82.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_82.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[3]->Draw("colz");
   hThetaPhi[3]->GetXaxis()->CenterTitle(true);
   hThetaPhi[3]->GetYaxis()->CenterTitle(true);
   hThetaPhi[3]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[3]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_83.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_83.pdf",runnumber));

   c   = new TCanvas();
   hThetaPhi[4]->Draw("colz");
   hThetaPhi[4]->SetTitle("With cut");
   hThetaPhi[4]->GetXaxis()->CenterTitle(true);
   hThetaPhi[4]->GetYaxis()->CenterTitle(true);
   hThetaPhi[4]->GetXaxis()->SetTitle("#phi_{e} [deg]");
   hThetaPhi[4]->GetYaxis()->SetTitle("#theta_{e} [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_8.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_8.pdf",runnumber));

   //// ---------------------------------------------
   //c   = new TCanvas();
   //hThetaPhi[1]->Draw("colz");
   //hThetaPhi[6]->SetTitle("With cut");
   //hThetaPhi[1]->GetXaxis()->CenterTitle(true);
   //hThetaPhi[1]->GetYaxis()->CenterTitle(true);
   //hThetaPhi[1]->GetXaxis()->SetTitle("#phi_{p} [deg]");
   //hThetaPhi[1]->GetYaxis()->SetTitle("#theta_{p} [deg]");
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_81.png",runnumber));
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_81.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   hThetaPhi[6]->Draw("colz");
   hThetaPhi[6]->SetTitle("With cut");
   hThetaPhi[6]->GetXaxis()->CenterTitle(true);
   hThetaPhi[6]->GetYaxis()->CenterTitle(true);
   hThetaPhi[6]->GetXaxis()->SetTitle("#phi_{#gamma} [deg]");
   hThetaPhi[6]->GetYaxis()->SetTitle("#theta_{#gamma} [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_82.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_82.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   hThetaPhi[7]->Draw("colz");
   hThetaPhi[7]->SetTitle("With cut");
   hThetaPhi[7]->GetXaxis()->CenterTitle(true);
   hThetaPhi[7]->GetYaxis()->CenterTitle(true);
   hThetaPhi[7]->GetXaxis()->SetTitle("#phi_{He4} [deg]");
   hThetaPhi[7]->GetYaxis()->SetTitle("#theta_{He4} [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_83.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_83.pdf",runnumber));

   // ---------------------------------------------
   // Theta vs P
   c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[0]->Draw("colz");
   hThetaP[0]->GetXaxis()->CenterTitle(true);
   hThetaP[0]->GetYaxis()->CenterTitle(true);
   hThetaP[0]->GetXaxis()->SetTitle("P [GeV/c]");
   hThetaP[0]->GetYaxis()->SetTitle("#theta_{e} [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_30.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_30.pdf",runnumber));

   //// ---------------------------------------------
   //c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   //hThetaP[1]->Draw("colz");
   //hThetaP[1]->GetXaxis()->CenterTitle(true);
   //hThetaP[1]->GetYaxis()->CenterTitle(true);
   //hThetaP[1]->GetXaxis()->SetTitle("P [GeV/c]");
   //hThetaP[1]->GetYaxis()->SetTitle("#theta_{p} [deg]");
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_81.png",runnumber));
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_81.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaP[2]->Draw("colz");
   hThetaP[2]->GetXaxis()->CenterTitle(true);
   hThetaP[2]->GetYaxis()->CenterTitle(true);
   hThetaP[2]->GetXaxis()->SetTitle("P [GeV/c]");
   hThetaP[2]->GetYaxis()->SetTitle("#theta_{#gamma} [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_32.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_32.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   hThetaP[3]->Draw("colz");
   hThetaP[3]->GetXaxis()->CenterTitle(true);
   hThetaP[3]->GetYaxis()->CenterTitle(true);
   hThetaP[3]->GetXaxis()->SetTitle("P [GeV/c]");
   hThetaP[3]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_33.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_33.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("phihists","#phi");
   for(int i = 0; i<4; i++) {
      hPhi[i]->SetLineColor(colors[i]);
      hs->Add(hPhi[i]);
   }
   hs->Draw("nostack");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_9.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_9.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaXhists","#Delta x");
   hx[2]->SetLineColor(1);
   hx[5]->SetLineColor(2);
   hs->Add(hx[2]);
   hs->Add(hx[5]);
   leg->AddEntry(hx[2],"all","l");
   leg->AddEntry(hx[5],"with cut","l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("(x_{thrown}-x_{recon})/x_{thrown}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_10.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_10.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.55, 0.7, 0.85, 0.9);
   hs  = new THStack("deltathists","#Delta t");
   ht[2]->SetTitle("#sigma t");
   ht[2]->SetLineColor(1);
   ht[5]->SetLineColor(2);
   hs->Add(ht[2]);
   hs->Add(ht[5]);
   leg->AddEntry(ht[2],"all","l");
   leg->AddEntry(ht[5],"with cut","l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("(t_{thrown}-t_{recon})/t_{thrown}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_11.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_11.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   hs  = new THStack("deltaQ2hists","#sigma Q^{2}");
   hQ2[2]->SetLineColor(1);
   hQ2[5]->SetLineColor(2);
   hs->Add(hQ2[2]);
   hs->Add(hQ2[5]);
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("(Q^{2}_{thrown}-Q^{2}_{recon})/Q^{2}_{thrown}");
   hs->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_12.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_12.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hW[2]->Draw();
   hW[2]->GetXaxis()->CenterTitle(true);
   hW[2]->GetYaxis()->CenterTitle(true);
   hW[2]->GetXaxis()->SetTitle("(W_{thrown}-W_{recon})/W_{thrown}");
   hW[2]->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_13.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_13.pdf",runnumber));
   // ---------------------------------------------
   //c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   //hDeltatVst[0]->Draw("colz");
   //hDeltatVst[0]->GetXaxis()->CenterTitle(true);
   //hDeltatVst[0]->GetYaxis()->CenterTitle(true);
   //hDeltatVst[0]->GetXaxis()->SetTitle("t_{recon/exact}");
   //hDeltatVst[0]->GetYaxis()->SetTitle("#Delta t (recon)");
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_25.png",runnumber));
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_25.pdf",runnumber));

   //// ---------------------------------------------
   //c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   //hDeltatVst[1]->Draw("colz");
   //hDeltatVst[1]->GetXaxis()->CenterTitle(true);
   //hDeltatVst[1]->GetYaxis()->CenterTitle(true);
   //hDeltatVst[1]->GetXaxis()->SetTitle("t_{recon/approx}");
   //hDeltatVst[1]->GetYaxis()->SetTitle("#Delta t (recon)");
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_26.png",runnumber));
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_26.pdf",runnumber));

   //// ---------------------------------------------
   //c   = new TCanvas();
   //leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   //hDeltatVst[2]->Draw("colz");
   //hDeltatVst[2]->GetXaxis()->CenterTitle(true);
   //hDeltatVst[2]->GetYaxis()->CenterTitle(true);
   //hDeltatVst[2]->GetXaxis()->SetTitle("t_{recon/exact}");
   //hDeltatVst[2]->GetYaxis()->SetTitle("t_{recon/approx}");
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_27.png",runnumber));
   //c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_27.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("asymHists","Asymmetry");

   hPhiAsym[0]->Sumw2();
   hPhiAsym[1]->Sumw2();
   TH1 * phi_asym = hPhiAsym[0]->GetAsymmetry(hPhiAsym[1]);
   phi_asym->SetLineColor(1);
   phi_asym->SetLineWidth(2);
   phi_asym->SetMarkerStyle(20);
   hs->Add(phi_asym);
   leg->AddEntry(phi_asym,"all","ep");

   hPhiAsym[2]->Sumw2();
   hPhiAsym[3]->Sumw2();
   phi_asym = hPhiAsym[2]->GetAsymmetry(hPhiAsym[3]);
   phi_asym->SetLineColor(2);
   phi_asym->SetLineWidth(2);
   phi_asym->SetMarkerStyle(20);
   phi_asym->SetMarkerColor(2);
   hs->Add(phi_asym);
   leg->AddEntry(phi_asym,"with cuts","ep");

   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#phi");
   hs->GetYaxis()->SetTitle("");
   hs->SetMinimum(-1);
   hs->SetMaximum(1);
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_28.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_28.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   hQ2Vsx[0]->Draw("colz");
   hQ2Vsx[0]->GetXaxis()->CenterTitle(true);
   hQ2Vsx[0]->GetYaxis()->CenterTitle(true);
   hQ2Vsx[0]->GetXaxis()->SetTitle("x");
   hQ2Vsx[0]->GetYaxis()->SetTitle("Q^{2}");
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_29.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/coherent_dvcs_He4_%d_29.pdf",runnumber));
}


