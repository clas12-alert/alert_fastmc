#ifndef CLAS12Resolutions_HH
#define CLAS12Resolutions_HH


//#define TORAD   degree
//#define TODEG   1.0/degree
#define TORAD    0.01745
#define TODEG   57.299578


// 
// From conf0.dat
//      FORWARD:
//      Delta_P=I0/I*rnorml[1]
//  	Neutral: P=P+rnorml[1]*sqrt(P)
//      Theta=Theta+Delta_Theta
//      Delta_Theta=sqrt(s1^2+(s2/p/betta)^2)
//      Phi=Phi+Delta_Phi
//      Delta_Ph=sqrt(s1^2+(s2/p/betta)^2)
//  
//    theta dependence Sigma1=FoMT1S1+FoMT1S2*theta+FoMT1S3*theta**2
//  |--------|--|--|--|------I-R-|-------------------------------------------------|
//  FoMT1S1     REA4  .20678E-3    0.20678E-03  Forward 1-st param for sigma_1       
//  FoMT1S2     REA4  -.57466E-5   -0.57466E-05 Forward 2-d param for sigma_1
//  FoMT1S3     REA4  .38913E-6    0.38913E-06  Forward 3-d param for sigma_1 
//  FoMT2S1     REA4  .26652E-2    0.26652E-02  Forward 1-st param for sigma_1       
//  FoMT2S2     REA4  -.80631E-4   -0.80631E-04 Forward 2-d param for sigma_1
//  FoMT2S3     REA4  .87599E-5    0.87599E-05  Forward 3-d param for sigma_1 
//  FoInThS1    REA4  3.684E-05    3.684E-05    Forward inbending(outbending)  Theta sigma_1
//  FoInThS2    REA4  .00162613    0.00162613   Forward inbending(outbending)  Theta sigma_2
//  
//  
//  FoInMoS1    REA4  .005       Forward inbending mometum sigma_1
//  FoInMoS2    REA4  .0         Forward inbending mometum sigma_2
//  FoOuMoS1    REA4  .007       Forward outbending mometum sigma_1
//  FoOuMoS2    REA4  .0         Forward outbending mometum sigma_2
//  FoNeMoS1    REA4  .050       Forward neutral mometum sigma_1
//  FoNeMoS2    REA4  .0         Forward neutral mometum sigma_2
//  
//  FoOuThS1    REA4  .0286      Forward outbending Theta sigma_1
//  FoOuThS2    REA4  .00        Forward outbending Theta sigma_2
//  FoNeThS1    REA4  .0286      Forward neutral    Theta sigma_1
//  FoNeThS2    REA4  .00        Forward neutral    Theta sigma_2
//  
//    c      May 28 2005. K.Mikhailov: add new parameters to conf-files
//    FoIOFiS1    REA4  .683E-4    Forward in(out)bending  phi sigma_1
//    May 31, new parametrization from Gail:
//  FoA0FiS1    REA4  .3125E-2   Forward in(out)bending  A0 phi sigma_1
//  FoA1FiS1    REA4  .262E-4    Forward in(out)bending  A1 phi sigma_1
//  FoA2FiS1    REA4  -.350E-6   Forward in(out)bending  A2 phi sigma_1
//  
//  FoA0FiS2    REA4  .542E-3    Forward in(out)bending  A0 phi sigma_2
//  FoA1FiS2    REA4  -.531E-5   Forward in(out)bending  A1 phi sigma_2
//  FoA2FiS2    REA4  .886E-7    Forward in(out)bending  A2 phi sigma_2
//  FoNeFiS1    REA4  .0573      Forward neutral    phi sigma_1
//  FoNeFiS2    REA4  .0         Forward neutral    phi sigma_2
//  
//  |--------|--|--|--|------I-R-|-------------------------------------------------|
//      CENTRAL:
//  
//      Delta_P=rnorml[1]*sqrt((s1*P)^2+(s2/betta)^2)
//  	Neutral: P=P+rnorml[1]*sqrt(P)
//      Theta=Theta+Delta_Theta
//      Delta_Theta=sqrt(s1^2+(s2/p/betta)^2)
//      Phi=Phi+Delta_Phi
//      Delta_Ph=sqrt(s1^2+(s2/p/betta)^2)
//  
//  CeInMoS1    REA4  .065       Central inbending mometum sigma_1
//  CeInMoS2    REA4  .0138      Central inbending mometum sigma_2
//  CeOuMoS1    REA4  .065       Central outbending mometum sigma_1
//  CeOuMoS2    REA4  .0138      Central outbending mometum sigma_2
//  CeNeMoS1    REA4  .050       Central neutral mometum sigma_1
//  CeNeMoS2    REA4  .0         Central neutral mometum sigma_2
//  
//     May 31, 2005: sigma_2 = a0 + a1*theta + a2*theta^2, add new params.
//  CeA0ThS2    REA4  .4236E-3   Central in(out)bending  A0  Theta sigma_2
//  CeA1ThS2    REA4  -.4451E-5  Central in(out)bending  A1  Theta sigma_2
//  CeA2ThS2    REA4  .247E-7    Central in(out)bending  A2  Theta sigma_2
//  
//  CeInThS1    REA4  .8776E-2   Central in(out)bending  Theta sigma_1
//  
//  CeInThS2    REA4  .00000     Central inbending  Theta sigma_2
//  CeOuThS1    REA4  .0286      Central outbending Theta sigma_1
//  CeOuThS2    REA4  .00000     Central outbending Theta sigma_2
//  CeNeThS1    REA4  .0286      Central neutral    Theta sigma_1
//  CeNeThS2    REA4  .00000     Central neutral    Theta sigma_2
//  
//  CeInFiS1    REA4  .0069      Central inbending  phi sigma_1
//  CeInFiS2    REA4  .0074      Central inbending  phi sigma_2
//  CeOuFiS1    REA4  .0069      Central outbending phi sigma_1
//  CeOuFiS2    REA4  .0074      Central outbending phi sigma_2
//  CeNeFiS1    REA4  .0573      Central neutral    phi sigma_1
//  CeNeFiS2    REA4  .00000     Central neutral    phi sigma_2
//           1         2         3         4         5         6         7         8
//  ---------|---------|---------|---------|---------|---------|---------|----------



//  c  since our acceptance functions care about sagitta and not momentum, per se,
//  c  we will form a "normalized momentum", pnorm
//  cmac   this normalized momentum is only used for the fwd. tracker
//        pnorm=p*Tmax/abs(torcur)
//  c
//        thr=thetad*d2r
//        phir=phid*d2r
//  c     select which of the 6 functions we want
//        if(inbend.and.fwd)then
//           sig1p=FoInMoS1*Tmax/torcur
//           sig2p=FoInMoS2*Tmax/torcur
//           sig1th=FoInThS1
//           sig2th=FoInThS2
//           sig1fi=2.*sig1th
//           sig2fi=2.*sig2th
//           pout=p+ran(1)*p*sqrt((sig1p*p)**2+(sig2p/beta)**2)
//           thout=thr+ran(2)*sqrt(sig1th**2+(sig2th/p/beta)**2)
//           phiout=phir+ran(3)*sqrt(sig1fi**2+(sig2fi/p/beta)**2)
//  c     
//        elseif(central.AND..NOT.neutral)then
//           sig1p=CeInMoS1
//           sig2p=CeInMoS2
//           sig1th=CeInThS1*(sin(thr))**2
//           sig2th=CeInThS2*(sin(thr))**2
//           sig1fi=CeInFiS1
//           sig2fi=CeInFiS2
//           pperp=p*sin(thr)
//           dpperp=p*sqrt((CeInMoS1*p)**2+(CeInMoS2/beta)**2)
//           pperpout=pperp+ran(1)*dpperp
//           thout=thr+ran(2)*sqrt(sig1th**2+(sig2th/p/beta)**2)
//           pout=pperpout/sin(thout)
//           phiout=phir+ran(3)*sqrt(sig1fi**2+(sig2fi/p/beta)**2)
//  c     
//        elseif(outbend.and.fwd)then
//           sig1p=FoOuMoS1*Tmax/torcur
//           sig2p=FoOuMoS2*Tmax/torcur
//           sig1th=FoOuThS1
//           sig2th=FoOuThS2
//           sig1fi=2.*sig1th
//           sig2fi=2.*sig2th
//           pout=p+ran(1)*p*sqrt((sig1p*p)**2+(sig2p/beta)**2)
//           thout=thr+ran(2)*sqrt(sig1th**2+(sig2th/p/beta)**2)
//           phiout=phir+ran(3)*sqrt(sig1fi**2+(sig2fi/p/beta)**2)
//  c     
//        elseif(neutral.and.fwd)then
//           pout=p+ran(1)*sqrt(p)*FoNeMoS1
//           thout=thr+ran(2)*FoNeThS1
//           phiout=phir+ran(3)*FoNeFiS1
//  c     
//        elseif(neutral.and.central)then
//           pout=p+ran(1)*sqrt(p)*CeNeMoS1
//           thout=thr+ran(2)*CeNeThS1
//           phiout=phir+ran(3)*CeNeFiS1
//        endif
//  c  convert from radians to degrees
//        thout=thout*r2d
//        phiout=phiout*r2d

namespace clas12 {
   int SmearThrownParticle(const TParticle& thrown, TParticle& smeared) {
      using namespace TMath;
      // assuming out-bend and forward

      double rand = gRandom->Gaus();

      TLorentzVector p_0;
      thrown.Momentum(p_0);
      TLorentzVector p_1;
      thrown.Momentum(p_1);
    
      //std::cout<<thrown.GetPdgCode()<<endl;

      //  FoOuMoS1    REA4  .007       Forward outbending mometum sigma_1
      //  FoOuMoS2    REA4  .0         Forward outbending mometum sigma_2
      //  FoOuThS1    REA4  .0286      Forward outbending Theta sigma_1
      //  FoOuThS2    REA4  .00        Forward outbending Theta sigma_2
      //  FoA0FiS1    REA4  .3125E-2   Forward in(out)bending  A0 phi sigma_1
      //  FoA1FiS1    REA4  .262E-4    Forward in(out)bending  A1 phi sigma_1
      //  FoA2FiS1    REA4  -.350E-6   Forward in(out)bending  A2 phi sigma_1
      //
    /*  
      std::vector<double> forward_pars = {
         0.007     ,  //Forward outbending mometum sigma_1
         0.0       ,  //Forward outbending mometum sigma_2
         0.0286    ,  //Forward outbending Theta sigma_1
         0.00      ,  //Forward outbending Theta sigma_2
         0.3125e-2 ,  //Forward in(out)bending  A0 phi sigma_1
         0.262e-4  ,  //Forward in(out)bending  A1 phi sigma_1
         -0.350e-6     //Forward in(out)bending  A2 phi sigma_1
      };

      //           sig1p=FoOuMoS1*Tmax/torcur
      //           sig2p=FoOuMoS2*Tmax/torcur
      //           sig1th=FoOuThS1
      //           sig2th=FoOuThS2
      //           sig1fi=2.*sig1th
      //           sig2fi=2.*sig2th
      //           pout=p+ran(1)*p*sqrt((sig1p*p)**2+(sig2p/beta)**2)
      //           thout=thr+ran(2)*sqrt(sig1th**2+(sig2th/p/beta)**2)
      //           phiout=phir+ran(3)*sqrt(sig1fi**2+(sig2fi/p/beta)**2)




      double sig1p  = forward_pars[0];// Tmax/torcur
      double sig2p  = forward_pars[1];// Tmax/torcur
      double sig1th = forward_pars[2];
      double sig2th = forward_pars[3];
      double sig1fi = 2.0*sig1th;
      double sig2fi = 2.0*sig2th;
    

      double P      = p_0.P();
      double Theta  = p_0.Theta();
      double Phi0   = p_0.Phi();
      double Beta   = p_0.Beta();

      double pout   = p    + gRandom->Gaus()*p*Sqrt((sig1p*p)*(sig1p*p)+(sig2p/beta)*(sig2p/beta));
      double thout  = thr  + gRandom->Gaus()*Sqrt(Power(sig1th*sig1th+(sig2th/p/beta),2));
      double phiout = phir + gRandom->Gaus()*Sqrt(Power(sig1fi*sig1fi+(sig2fi/p/beta),2));

       p_0.SetRho(   pout );
       p_0.SetTheta( thout );
       p_0.SetPhi(   phiout );


      smeared.SetMomentum(p_0);
      */

      // added by mhattawy from the old clas12 fast MC ----------------------
      // parameters:
      double fTTmax = 3375.,     fTor_Cur =  2250.,      fThf_lim = 40.; 
      double fFoMT1S1=0.20678E-3,fFoMT1S2= -0.57466E-5,fFoMT1S3=0.38913E-6,   fFoMT2S1=0.26652E-2,   fFoMT2S2=-0.80631E-4,  fFoMT2S3= 0.87599E-5;
      double fFoInMoS1= 0.005,   fFoInMoS2= 0.0 ,      fFoOuMoS1=0.007 ,      fFoOuMoS2=0.0 ,        fFoNeMoS1=0.050,       fFoNeMoS2= 0.0;
      double fFoInThS1=3.684E-05,fFoInThS2= 0.00162613,fFoOuThS1= 0.0286,     fFoOuThS2= 0.00,       fFoNeThS1=0.0286,      fFoNeThS2=0.00;
      double fFoA0FiS1=0.3125E-2,fFoA1FiS1= 0.262E-4,  fFoA2FiS1= -0.350E-6 , fFoA0FiS2=0.542E-3,    fFoA1FiS2=-0.531E-5,   fFoA2FiS2=0.886E-7;
      double fFoNeFiS1=0.0573,   fFoNeFiS2= 0.0,       fCeInMoS1= 0.065,      fCeInMoS2= 0.0138,     fCeOuMoS1=0.065,       fCeOuMoS2= 0.0138 ;
      double fCeNeMoS1= 0.050 ,  fCeNeMoS2=0.0,        fCeA0ThS2= 0.4236E-3,  fCeA1ThS2= -0.4451E-5, fCeA2ThS2= 0.247E-7 ,  fCeInThS1= 0.8776E-2;
      double fCeInThS2= 0.00000, fCeOuThS1= 0.0286,    fCeOuThS2=0.00000,     fCeNeThS1=0.0286,      fCeNeThS2=0.00000,     fCeInFiS1=0.0069;

      double P      = p_0.P();
      double Theta  = p_0.Theta()*TODEG;
      double Phi0   = p_0.Phi()*TODEG;
      double Beta   = p_0.Beta();

      //1. find the type of the particle 
      int   InBend = 0, OutBend = 0,  Neutral = 0,  Fwd = 0,  Central = 0; 
      //  6 functions: (inbend,outbend,neutral) track X (fwd,central) det.

      int Charge = int(thrown.GetPDG()->Charge()/3.0);
      //if(thrown.GetPdgCode() == 11 ) Charge = -1;
      //else if (thrown.GetPdgCode() == 22 ) Charge = 0 ;
      //else if (thrown.GetPdgCode() == 2212 ) Charge = 1;
     
     if(Charge == 0){
        Neutral = 1;
     }
     if(TMath::Abs(Charge) == 1) {
        if( (Charge*fTor_Cur) > 0.0 ){
           OutBend = 1;
        } else  {
           InBend  = 1;
        }
     }
     if(Theta <  fThf_lim){
        Fwd     = 1;
     }
     if(Theta >= fThf_lim){
        Central = 1;
     }

     double Pout, Thout, Phiout;
     double Thr, Phir;
     double Pnorm;
     double sig1p, sig2p, sig1th, sig2th, sig1fi, sig2fi;
     double Pperp, dPperp, Pperpout;
     double Erg, IC_Erg_Res, IC_Ang_Res;
     double Theta_IC_max(4.5), Theta_IC_min(2.5);
     double P_IC_Erg[4] = {0.0299700, -0.0076880, 0.00106800, -4.63600e-05};
     double P_IC_Ang[4] = {0.0572505, -0.0144679, 0.00204576, -9.26138e-05};  
      
     int   ithree(3);
     double ran[3];
     ran[0] = gRandom->Gaus(0,1);
     ran[1] = gRandom->Gaus(0,1);
     ran[2] = gRandom->Gaus(0,1);
   
     // since our acceptance functions care about sagitta and not momentum, per se,
     // we will form a "normalized momentum", pnorm
     //cmac this normalized momentum is only used for the fwd. tracker
   
     Pnorm = P*fTTmax/TMath::Abs(fTor_Cur);
     Thr   = Theta*TORAD;
     Phir  = Phi0*TORAD;
   
     // select which of the 6 functions we want
     if(InBend && Fwd) {

        //ckm 14-apr-2005 (fit on all thetas), theta, phi resolutions:
        sig1p = (fTTmax/fTor_Cur)*(fFoMT1S1 + fFoMT1S2*Theta + fFoMT1S3*(Theta)*(Theta));
        sig2p = (fTTmax/fTor_Cur)*(fFoMT2S1 + fFoMT2S2*Theta + fFoMT2S3*(Theta)*(Theta));

        //ckm   sigma_theta is at 5 degrees(as upper limit)
        sig1th = fFoInThS1;
        sig2th = fFoInThS2;

        sig1fi = (fFoA0FiS1/(Thr*TODEG)) + fFoA1FiS1 + fFoA2FiS1*(Thr*TODEG); 		//Theta in degrees
        sig2fi = fFoA0FiS2 + fFoA1FiS2*Thr*TODEG + fFoA2FiS2*(Thr*TODEG)*(Thr*TODEG);  	//Theta in degrees

        Pout   = P    + ((double)ran[0])*P*sqrt((sig1p*P)*(sig1p*P) + (sig2p/Beta)*(sig2p/Beta));
        Thout  = Thr  + ((double)ran[1])*sqrt((sig1th)*(sig1th)    + (sig2th/P/Beta)*(sig2th/P/Beta));
        // Phiout = Phir + ((double)ran[2])*sqrt((sig1fi)*(sig1fi)    + (sig2fi/P/Beta)*(sig2fi/P/Beta)); 
        Phiout = Phir + ((double)ran[2])*sqrt((sig1fi)*(sig1fi)    + (sig2fi/P/Beta)*(sig2fi/P/Beta)); 

     } else if(Central && !Neutral) {

        sig1p = fCeInMoS1;
        sig2p = fCeInMoS2;
        //ckm May 31, 2005: sigma_2 = a0 + a1*theta + a2*theta^2
        sig1th   = fCeInThS1*(sin(Thr))*(sin(Thr));
        sig2th   = fCeInThS2*(sin(Thr))*(sin(Thr));

        sig1fi   = (fFoA0FiS1/(Thr*TODEG)) + fFoA1FiS1 + fFoA2FiS1*(Thr*TODEG); // comment mohammad : fCeInFiS1;
        sig2fi   = fFoA0FiS2 + fFoA1FiS2*Thr*TODEG + fFoA2FiS2*(Thr*TODEG)*(Thr*TODEG); // comment mohammad fCeInFiS2;
        Pperp    = P*sin(Thr);
        //cmac         dpperp=p*sqrt((CeInMoS1*p)**2+(CeInMoS2/beta)**2)
        dPperp   = Pperp*sqrt((fCeInMoS1*P)*(fCeInMoS1*P) + (fCeInMoS2/Beta)*(fCeInMoS2/Beta));
        Pperpout = Pperp + ((double)ran[0])*dPperp;
        Thout    = Thr   + ((double)ran[1])*sqrt((sig1th)*(sig1th) + (sig2th/P/Beta)*(sig2th/P/Beta));
        Pout     = Pperpout/sin(Thout);
        // Phiout   = Phir + ((double)ran[2])*sqrt((sig1fi)*(sig1fi) + (sig2fi/P/Beta)*(sig2fi/P/Beta));
        Phiout = Phir + ((double)ran[2])*sqrt((sig1fi)*(sig1fi)    + (sig2fi/P/Beta)*(sig2fi/P/Beta));

     } else if(OutBend && Fwd) {

        //ckm 14-apr-2005 (fit on all thetas), theta, phi resolutions:
        sig1p = fTTmax/fTor_Cur*(fFoMT1S1 + fFoMT1S2*Theta + fFoMT1S3*(Theta)*(Theta));
        sig2p = fTTmax/fTor_Cur*(fFoMT2S1 + fFoMT2S2*Theta + fFoMT2S3*(Theta)*(Theta));

        //ckm   sigma_theta is at 5 degrees(as upper limit)
        sig1th = fFoInThS1;
        sig2th = fFoInThS2;

        //km May 31, 2005
        sig1fi = fFoA0FiS1/(Thr*TODEG) + fFoA1FiS1 + fFoA2FiS1*(Thr*TODEG);  //Theta in degrees
        sig2fi = fFoA0FiS2 + fFoA1FiS2*Thr*TODEG + fFoA2FiS2*(Thr*TODEG)*(Thr*TODEG); //Theta in degrees

        Pout   = P    + ((double)ran[0])*P*sqrt((sig1p*P)*(sig1p*P) + (sig2p/Beta)*(sig2p/Beta));
        Thout  = Thr  + ((double)ran[1])*sqrt((sig1th)*(sig1th)    + (sig2th/P/Beta)*(sig2th/P/Beta));
        // Phiout = Phir + ((double)ran[2])*sqrt((sig1fi)*(sig1fi)    + (sig2fi/P/Beta)*(sig2fi/P/Beta));
        Phiout = Phir +  ((double)ran[2])*sqrt((sig1fi)*(sig1fi)    + (sig2fi/P/Beta)*(sig2fi/P/Beta));

     } else if(Neutral && Fwd) {

        if(Theta > Theta_IC_max) {
           // Neutral in FWD EC
           Pout   = P    + ((double)ran[0])*sqrt(P)*fFoNeMoS1;
           Thout  = Thr  + ((double)ran[1])*fFoNeThS1;
           //Phiout = Phir + ((double)ran[2])*fFoNeFiS1;
           Phiout =Phir +   ((double)ran[2])*fFoNeFiS1;

        } else if(Theta > Theta_IC_min && Theta < Theta_IC_max) {

           // Neutral in IC 
           if(Beta > 0 && Beta < 1) Erg = P/Beta;
           else Erg = P;

           IC_Erg_Res=Erg*sqrt((0.02/Erg)*(0.02/Erg) + 0.01*0.01);
           IC_Ang_Res = 57.2557*0.2/(186*sqrt(Erg));

           ///// Smear energy and angles with a gaussian
           Pout   = P    + ((double)ran[0])*IC_Erg_Res; 
           Thout  = Thr  + ((double)ran[1])*IC_Ang_Res*TORAD;
           //Phiout = Phir + ((double)ran[2])*IC_Ang_Res*TORAD;	
           Phiout = Phir +  ((double)ran[2])*IC_Ang_Res*TORAD;
        }

     } else if(Neutral && Central) { 

        Pout   = P    + ((double)ran[0])*sqrt(P)*fCeNeMoS1;
        Thout  = Thr  + ((double)ran[1])*fCeNeThS1;
        // Phiout = Phir + ((double)ran[2])*fCeNeFiS1;
        Phiout =  Phir + ((double)ran[2])*((fFoA0FiS1/(Thr*TODEG)) + fFoA1FiS1 + fFoA2FiS1*(Thr*TODEG)); //  *fCeNeFiS1;
        //std::cout << " phi = " << Phiout << std::endl;

     }

     p_0.SetRho(  Pout  );
     p_0.SetTheta(Thout );
     p_0.SetPhi(  Phiout);
     //std::cout << Phiout << std::endl;
     //std::cout<< Pout - P<<endl;
     //   std::cout << " phi = " << Phiout/degree << std::endl;

     smeared.SetMomentum(p_0);


     return 0;
   }
}

#endif

